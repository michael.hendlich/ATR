package lu.uni.lcsb.atr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by sysadmin on 13.08.15.
 */
public class SharedFunctionality {

    public static void createProject(String name, File metaFile) {
        DataProvider.project = new Project(null, name, null);
        FileIO.saveProject(metaFile);
    }

    public static SelectionTable createTable(int start, int end, FieldNameSet domain) {
        Selection s = new Selection(start, end, domain);
        SelectionTable table = new SelectionTable(s);
        DataProvider.project.getTables().add(table);
        return table;
    }

    public static void applyTool(String toolName, ToolMode toolMode, String params) {
        Tools.onToolUse(toolName, toolMode, params);
    }

    public static void createModifiedDD(ArrayList<SelectionTable> tables) {
        ArrayList<ArrayList<String>> temp = FileIO.readCSVFile(DataProvider.dataDictionary);

        for (SelectionTable table : tables) {

            ArrayList<String> ins = new ArrayList<>();
            for (Annotation a : table.getAnnotationSet().getList()) {
                ins.add(a.toFormattedString());
            }

            FileIO.modifyLoadedCSVFile(temp, ins, table.getSelection().getStartLine());
        }

        File out = new File("backups/out/out.csv");
        try {
            out.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileIO.writeCSVFile(temp, out);
        DataProvider.dataDictionaryEntries = FileIO.readCSVFile(out);

    }

    public static void changeDomain(SelectionTable table, FieldNameSet newDomain) {
        table.changeNameSet(newDomain);
    }

    public static boolean loadDD() {
        DataProvider.dataDictionaryEntries = FileIO.readCSVFile(DataProvider.dataDictionary);
        return DataProvider.dataDictionaryEntries != null && DataProvider.dataDictionaryEntries.get(0).size() >= 18;
    }

    public static boolean loadProject(File file) {
        try {
            DataProvider.project = FileIO.loadProject(file);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public static boolean loadProject(String name) {
        File metaFile = new File("Projects/" + name + "/" + name + DataProvider.DEFAULT_METADATA_FILE_EXTENSION);
        return loadProject(metaFile);
    }

    public static void saveProject(File file) {
        FileIO.saveProject(file);
    }

    public static void saveProject(String name) {
        File metaFile = new File("Projects/" + name + "/" + name + DataProvider.DEFAULT_METADATA_FILE_EXTENSION);
        saveProject(metaFile);
    }

    public static void downloadDD() {
        APIConnector.downloadDD();
    }

    public static boolean uploadDD() {
        return APIConnector.uploadDDSafe();
    }

    public static void executeToolsOnLoad() {
        Tools.onLoad();
    }

}
