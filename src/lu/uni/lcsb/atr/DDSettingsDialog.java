package lu.uni.lcsb.atr;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class DDSettingsDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtFieldDDPath;
    private JButton btnSearch;
    private JComboBox comboBoxColumn;
    private JCheckBox checkBoxOverwrite;
    private JButton btnDownload;

    private File dd;

    public DDSettingsDialog(Window window) {

        super(window);
        setModalityType(ModalityType.TOOLKIT_MODAL);

        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        initBtnSearch();
        initComboBoxColumn();
        initBtnDownload();
        if (DataProvider.dataDictionary != null) {
            txtFieldDDPath.setText(DataProvider.dataDictionary.getAbsolutePath());
        }

        checkBoxOverwrite.setSelected(DataProvider.alwaysOverwrite);

        pack();
        setLocationRelativeTo(null);

    }

    private void initBtnDownload() {
        btnDownload.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                APIConnector.downloadDD();
                dd = Utils.getNewestInFile();
                txtFieldDDPath.setText(dd.getAbsolutePath());
            }
        });
    }

    private void initComboBoxColumn() {
        comboBoxColumn.removeAllItems();
        for (String s : DataProvider.dataDictionaryHeaderMapping) {
            comboBoxColumn.addItem(s);
        }
        comboBoxColumn.setSelectedIndex(DataProvider.dataDictionaryHeaderMapping.length-1);
    }



    private void initBtnSearch() {
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.addChoosableFileFilter(new CSVFileFilter());
                fileChooser.setAcceptAllFileFilterUsed(true);
                if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    dd = fileChooser.getSelectedFile();
                    txtFieldDDPath.setText(dd.getAbsolutePath());
                }
            }
        });
    }

    private void onOK() {
        if (dd != null) {
            DataProvider.dataDictionary = dd;
        }

        DataProvider.columnToInsert = comboBoxColumn.getSelectedIndex();
        DataProvider.alwaysOverwrite = checkBoxOverwrite.isSelected();
        dispose();
    }


    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
