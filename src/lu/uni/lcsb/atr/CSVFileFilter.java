package lu.uni.lcsb.atr;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Created by sysadmin on 23.07.15.
 */
public class CSVFileFilter extends FileFilter {
    @Override
    public boolean accept(File file) {
        return file.getAbsolutePath().endsWith(DataProvider.DEFAULT_CSV_FILE_EXTENSION) || file.isDirectory();
    }

    @Override
    public String getDescription() {
        return ".csv (comma separated values)";
    }
}
