package lu.uni.lcsb.atr;

import ext.TextPrompt;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class DataDictionaryDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JScrollPane scrollPane;
    private JTable table;
    private JList list;
    private JComboBox<String> comboBoxDomains;
    private JButton btnAddSelection;
    private JButton btnDeleteSelection;
    private JButton btnCreateDomain;
    private JButton btnImportDomain;
    private JButton btnMoveUp;
    private JButton btnMoveDown;
    private JButton btnSwitchMode;
    private JTextField txtFieldSearchString;
    private JComboBox<String> comboBoxSearchColumn;
    private JButton btnSearch;
    private JButton btnSearchUp;
    private JButton btnSearchDown;
    private JLabel lblCurrIndex;

    private DDTableModel tableModel;
    private CustomListModel listModel;
    private ArrayList<ArrayList<String>> dataDictionary;

    private String[] simpleModelHeaders;
    private final int [] simpleModelHeadersIndexes = {0, 1, 2, 3, 17};

    private ArrayList<Integer> foundIndexes;
    private int currentFoundIndex = 0;

    private ArrayList<Selection> selections;

    private String tableViewMode = "extended";

    private FlowCode flowCode;

    public DataDictionaryDialog(Window window) {

        super(window);
        setModalityType(ModalityType.APPLICATION_MODAL);

        init();

        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        initTable();
        initList();
        initBtnImportDomain();
        initComboBoxDomains();
        initComboBoxSearchColumn();
        initBtnCreateDomain();
        initBtnAddSelection();
        initBtnDeleteSelection();
        initBtnMoveUp();
        initBtnMoveDown();
        initBtnSwitchMode();
        initBtnSearch();
        initBtnSearchUp();
        initBtnSearchDown();

        TextPrompt tp = new TextPrompt("search term", txtFieldSearchString);
        tp.changeAlpha(0.5f);

        pack();
        setLocationRelativeTo(null);

    }

    private void initBtnSearchDown() {
        btnSearchDown.setEnabled(false);
        btnSearchDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                currentFoundIndex++;
                if (currentFoundIndex > foundIndexes.size() - 1) {
                    currentFoundIndex = foundIndexes.size() - 1;
                }
                searchPositionChanged();
            }
        });
    }

    private void initBtnSearchUp() {
        btnSearchUp.setEnabled(false);
        btnSearchUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                currentFoundIndex--;
                if (currentFoundIndex < 0) {
                    currentFoundIndex = 0;
                }
                searchPositionChanged();
            }
        });
    }

    private void searchPositionChanged() {
        table.scrollRectToVisible(table.getCellRect(foundIndexes.get(currentFoundIndex), 0, true));
        lblCurrIndex.setText((currentFoundIndex + 1) + " / " + foundIndexes.size());
        repaint();
    }

    private void initBtnSearch() {
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String toSearch = txtFieldSearchString.getText();
                if (toSearch != null && !toSearch.equals("")) {

                    toSearch = toSearch.toLowerCase();

                    int columnIndex = comboBoxSearchColumn.getSelectedIndex() + 1;
                    ArrayList<String> column = new ArrayList<>(dataDictionary.size());
                    for (ArrayList<String> rows : dataDictionary) {
                        column.add(rows.get(columnIndex).toLowerCase());
                    }

                    foundIndexes = new ArrayList<>();
                    currentFoundIndex = 0;
                    for (int i = 0; i < column.size(); i++) {
                        if (column.get(i).contains(toSearch)) {
                            foundIndexes.add(i);
                        }
                    }

                    if (foundIndexes == null || foundIndexes.size() == 0) {
                        btnSearchUp.setEnabled(false);
                        btnSearchDown.setEnabled(false);
                        lblCurrIndex.setText("");
                        JOptionPane.showMessageDialog(DataDictionaryDialog.this, "Nothing found.");

                    } else {
                        btnSearchUp.setEnabled(true);
                        btnSearchDown.setEnabled(true);
                        searchPositionChanged();
                    }

                }
            }
        });
    }

    private void initComboBoxSearchColumn() {
        comboBoxSearchColumn.removeAllItems();
        if (tableViewMode.equals("simple")) {
            for (String s : simpleModelHeaders) {
                comboBoxSearchColumn.addItem(s);
            }
        } else if (tableViewMode.equals("extended")) {
            for (String s : DataProvider.dataDictionaryHeaderMapping) {
                comboBoxSearchColumn.addItem(s);
            }
        }

    }

    private void initBtnSwitchMode() {
        btnSwitchMode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (tableViewMode.equals("simple")) {
                    tableViewMode = "extended";
                    btnSwitchMode.setText("Switch To Simple Table");
                } else if (tableViewMode.equals("extended")) {
                    tableViewMode = "simple";
                    btnSwitchMode.setText("Switch To Extended Table");
                }
                initTableMode();
                initComboBoxSearchColumn();
            }
        });
    }

    private void initBtnMoveDown() {
        btnMoveDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int i = list.getSelectedIndex();
                if (i < selections.size() -1) {
                    Selection s = selections.get(i);
                    selections.remove(i);
                    selections.add(i + 1, s);
                    list.setSelectedIndex(i + 1);
                    listModel.update();
                }
            }
        });
    }

    private void initBtnMoveUp() {
        btnMoveUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int i = list.getSelectedIndex();
                if (i > 0) {
                    Selection s = selections.get(i);
                    selections.remove(i);
                    selections.add(i - 1, s);
                    list.setSelectedIndex(i - 1);
                    listModel.update();
                }
            }
        });
    }

    private void initBtnDeleteSelection() {
        btnDeleteSelection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int i = list.getSelectedIndex();
                DataProvider.project.getTables().remove(Utils.findSelectionTableBySelection(selections.get(i)));
                selections.remove(i);
                listModel.update();
                repaint();
            }
        });
    }

    private void init() {
        initTableMode();
        selections = new ArrayList<>();
    }

    private void initTableMode() {
        simpleModelHeaders = new String[simpleModelHeadersIndexes.length];
        for (int i = 0; i < simpleModelHeaders.length; i++) {
            simpleModelHeaders[i] = DataProvider.dataDictionaryHeaderMapping[simpleModelHeadersIndexes[i]];
        }

        if (tableViewMode.equals("simple")) {
            loadDDSimple();
        } else if (tableViewMode.equals("extended")) {
            loadDDExtended();
        }

        if (tableModel != null) {
            tableModel.reconstruct();
        }

    }

    private void loadDDExtended() {
        this.dataDictionary = new ArrayList<>();
        for (ArrayList<String> list : DataProvider.dataDictionaryEntries) {
            ArrayList<String> row = new ArrayList<>();
            for (String s : list) {
                row.add(new String(s));
            }
            dataDictionary.add(row);
        }

        for (int i = 0; i < dataDictionary.size(); i++) {
            dataDictionary.get(i).add(0, Integer.toString(i));
        }
    }

    private void loadDDSimple() {
        this.dataDictionary = new ArrayList<>();
        for (ArrayList<String> list : DataProvider.dataDictionaryEntries) {
            ArrayList<String> row = new ArrayList<>();
            java.util.List<String> headers = Arrays.asList(simpleModelHeaders);
            for (int i = 0; i < list.size(); i++) {
                if (headers.contains(DataProvider.dataDictionaryHeaderMapping[i])) {
                    row.add(new String(list.get(i)));
                }
            }
            dataDictionary.add(row);
        }

        for (int i = 0; i < dataDictionary.size(); i++) {
            dataDictionary.get(i).add(0, Integer.toString(i));

            if (!dataDictionary.get(i).get(simpleModelHeaders.length).equals("")) {
                dataDictionary.get(i).set(simpleModelHeaders.length, "YES");
            } else {
                dataDictionary.get(i).set(simpleModelHeaders.length, null);
            }
        }
    }


    private void initBtnAddSelection() {
        btnAddSelection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int[] i = table.getSelectedRows();
                if (Utils.itemAlreadySelected(selections, i)) {
                    JOptionPane.showMessageDialog(null, "You can't use the same row in multiple selections");
                } else {
                    SelectionTable t = SharedFunctionality.createTable(i[0], i[i.length - 1], Utils.getNameSetByID(comboBoxDomains.getSelectedItem().toString()));
                    selections.add(t.getSelection());
                    listModel.update();
                }
                repaint();

            }
        });
    }

    private void initBtnCreateDomain() {
        btnCreateDomain.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                FieldNameSetCreator creator = new FieldNameSetCreator(null);
                creator.setVisible(true);
                update();
            }
        });
    }

    private void initBtnImportDomain() {
        btnImportDomain.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Utils.importDomainFile();
                update();
            }
        });
    }

    private void initComboBoxDomains() {
        comboBoxDomains.setRenderer(new CustomCBRenderer());
        comboBoxDomains.removeAllItems();
        for (FieldNameSet set : DataProvider.loadedNameSets) {
            comboBoxDomains.addItem(set.getID());
        }
    }

    private void update() {
        initComboBoxDomains();
    }

    private void initList() {
        listModel = new CustomListModel();
        list.setModel(listModel);
        list.setCellRenderer(new CustomListRenderer());
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setLayoutOrientation(JList.VERTICAL);
    }

    private void initTable() {
        tableModel = new DDTableModel();
        table.setModel(tableModel);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setDefaultRenderer(String.class, new CustomCellRenderer());

        table.setRowSelectionAllowed(true);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    }

    private void onOK() {
        flowCode = FlowCode.openAnnotationWindow;
        dispose();
    }

    private void onCancel() {
        int i = JOptionPane.showConfirmDialog(
                this,
                "You will lose all unsaved data. Continue?",
                "Warning",
                JOptionPane.YES_NO_OPTION);
        if (i == JOptionPane.OK_OPTION) {
            flowCode = FlowCode.exit;
            dispose();
        }

    }


    public FlowCode getFlowCode() {
        return flowCode;
    }

    private class CustomCBRenderer extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

            if (!isSelected) {
                String s = String.valueOf(value);
                c.setBackground(Utils.getAssignedColor(Utils.getNameSetByID(s)));
            } else {
                String s = String.valueOf(value);
                Color color = Utils.getAssignedColor(Utils.getNameSetByID(s));
                c.setBackground(Utils.toDarkerColor(color));
            }

            return c;
        }
    }

    private class CustomListRenderer extends DefaultListCellRenderer {
        public Component getListCellRendererComponent( JList list, Object value, int index, boolean isSelected, boolean cellHasFocus ) {
            Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

            if (!isSelected) {
                Selection s = selections.get(index);
                Color color = Utils.getAssignedColor(s.getNameSet());

                c.setBackground(color);
            } else {
                Selection s = selections.get(index);
                Color color = Utils.getAssignedColor(s.getNameSet());

                c.setBackground(Utils.toDarkerColor(color));
            }

            return c;
        }
    }

    private class CustomListModel extends AbstractListModel {

        @Override
        public int getSize() {
            return selections.size();
        }

        @Override
        public Object getElementAt(int i) {
            return selections.get(i);
        }

        public void update() {
            fireContentsChanged(this,0,getSize()-1);
        }

    }

    private class CustomCellRenderer extends DefaultTableCellRenderer {

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

            Component rendererComp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            rendererComp.setForeground(Color.BLACK);

            if (table.isRowSelected(row)) {
                rendererComp.setBackground(Utils.getAssignedColor(Utils.getNameSetByID(comboBoxDomains.getSelectedItem().toString())));
            } else {
                int[] i  = {row};
                if (Utils.itemAlreadySelected(selections, i)) {
                    Selection s = Utils.getSelectionOfRow(selections, row);
                    rendererComp.setBackground(Utils.getAssignedColor(s.getNameSet()));
                } else {
                    rendererComp.setBackground(Color.white);
                }
            }

            if (foundIndexes != null && foundIndexes.size() > 0) {

                for (int i : foundIndexes) {
                    if (row == i && column == 0) {
                        rendererComp.setBackground(new Color(172, 229, 255));
                    }
                }

                if (row == foundIndexes.get(currentFoundIndex) && column == 0) {
                    rendererComp.setBackground(new Color(255, 150, 138));
                }
            }

            /*
            if (row % 2 == 0) {
                rendererComp.setBackground(new Color(240, 240, 240));
            } else {
                rendererComp.setBackground(Color.WHITE);
            }
            */
            return rendererComp;

        }

    }

    private class DDTableModel extends AbstractTableModel {

        @Override
        public String getColumnName(int column) {
            if (column == 0) {
                return "Row number";
            } else {
                if (tableViewMode.equals("simple")) {
                    return simpleModelHeaders[column-1];
                } else if (tableViewMode.equals("extended")) {
                    return DataProvider.dataDictionaryHeaderMapping[column-1];
                }

            }
            return null;
        }

        @Override
        public int getRowCount() {
            return dataDictionary.size();
        }

        @Override
        public int getColumnCount() {
            return dataDictionary.get(0).size();
        }

        @Override
        public Object getValueAt(int i, int i1) {
            return dataDictionary.get(i).get(i1);
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

        public void reconstruct() {
            fireTableStructureChanged();
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

    }
}
