package lu.uni.lcsb.atr;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Created by sysadmin on 31.07.15.
 */
public class ATRFileFilter extends FileFilter {
    @Override
    public boolean accept(File file) {
        return file.getAbsolutePath().endsWith(DataProvider.DEFAULT_METADATA_FILE_EXTENSION) || file.isDirectory();
    }

    @Override
    public String getDescription() {
        return "ATR Project File (*.atr)";
    }
}
