package lu.uni.lcsb.atr;

/**
 * Created by sysadmin on 21.07.15.
 */
public class Tuple implements Comparable {

    private String left;
    private String right;
    private FieldNameSet nameSet;

    public Tuple(String left, String right, FieldNameSet nameSet) {
        this.left = left;
        this.right = right;
        this.nameSet = nameSet;
    }

    public String getLeft() {
        return left;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    @Override
    public int compareTo(Object o) {
        Tuple other = (Tuple) o;
        if (other.getNameSet() != getNameSet()) {
            System.err.println("This should never ever happen");
        }
        return nameSet.getSortPosition(getLeft()) - nameSet.getSortPosition(other.getLeft());
    }

    public FieldNameSet getNameSet() {
        return nameSet;
    }
}
