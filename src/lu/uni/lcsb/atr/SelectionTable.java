package lu.uni.lcsb.atr;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;

/**
 * Created by sysadmin on 30.07.15.
 */
public class SelectionTable extends JPanel {

    private AnnotationSet annotationSet;
    private Selection selection;

    private JButton button;

    private JTable table;
    private TableModel model;
    private ToolTipHeader header;
    private CustomCellRenderer renderer;

    private int lastSelectedRow = -1;
    private int lastSelectedColumn = -1;
    private boolean lastSelectedTable = false;

    public SelectionTable(Selection selection) {
        this.selection = selection;
        annotationSet = Utils.selectionToAnnotationSet(DataProvider.dataDictionaryEntries, selection);

        init();
    }

    public SelectionTable(Selection selection, AnnotationSet annotationSet) {
        this.selection = selection;
        this.annotationSet = annotationSet;

        init();
    }

    private void init() {
        BoxLayout layout = new BoxLayout(this, BoxLayout.PAGE_AXIS) {

        };
        setLayout(layout);
        setVisible(true);

        //setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        //setSize(200, 200);
        //setBackground(Utils.randColor());
        initButton();
        initTable();

        //add(button);
        //add(table);
        JPanel panel = new JPanel();
        BoxLayout bl = new BoxLayout(panel, BoxLayout.X_AXIS);
        panel.setLayout(bl);
        panel.add(button);
        add(panel);
        add(table.getTableHeader());
        add(table);

        //revalidate();
    }

    public void changeNameSet(FieldNameSet nameSet) {
        annotationSet.changeNameSet(nameSet);
        selection.setNameSet(nameSet);
        button.setText(selection.toString());
        button.setBackground(Utils.getAssignedColor(selection.getNameSet()));
        update();
    }

    public void modifyData(String[][] newData) {
        for (int row = 0; row < annotationSet.getSize(); row++) {
            for (int column = 0; column < annotationSet.getFieldNameSet().getSize(); column++) {
                annotationSet.getList().get(row).updateTuple(column, newData[row][column]);
            }
        }
        update();
    }

    public void update() {
        model.reconstruct();
        revalidate();
        repaint();
    }

    private void initTable() {
        table = new JTable() {
            @Override
            public String getToolTipText(MouseEvent event) {
                Point p = event.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);

                return createToolTipText(rowIndex, colIndex);
            }
        };
        model = new TableModel();

        header = new ToolTipHeader(table.getColumnModel());
        header.setToolTipText("");
        table.setTableHeader(header);
        ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);

        table.setModel(model);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setDefaultRenderer(String.class, new CustomCellRenderer());

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                updateTableSelection(evt);
            }

            @Override
            public void mousePressed(MouseEvent evt) {
                updateTableSelection(evt);
            }

            private void updateTableSelection(MouseEvent evt) {
                lastSelectedRow = table.rowAtPoint(evt.getPoint());
                lastSelectedColumn = table.columnAtPoint(evt.getPoint());
                Utils.resetAllTableSelections();
                lastSelectedTable = true;
                getParent().repaint();
            }
        });

        ActionListener copyListener = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                doCopy();
            }
        };
        final KeyStroke copyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK, false);
        table.registerKeyboardAction(copyListener, "Copy", copyStroke, JComponent.WHEN_FOCUSED);

        ActionListener pasteListener = new ActionListener(){
            public void actionPerformed(ActionEvent event) {
                doPaste();
            }
        };
        final KeyStroke pasteStroke = KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK, false);
        table.registerKeyboardAction(pasteListener, "Paste", pasteStroke, JComponent.WHEN_FOCUSED);

        ActionListener deleteListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                doDelete();
            }
        };
        final KeyStroke deleteStroke = KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0);
        table.registerKeyboardAction(deleteListener, "Delete", deleteStroke, JComponent.WHEN_FOCUSED);

        ListSelectionModel selModel = table.getSelectionModel();
        selModel.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        table.setVisible(true);

    }

    private String createToolTipText(int rowIndex, int colIndex) {
        if (colIndex == 1) {

            if (DataProvider.dataDictionaryEntries == null) {
                return "";
            }

            StringBuilder sb = new StringBuilder();
            sb.append("<html>");

            for (int i = 0; i < DataProvider.dataDictionaryHeaderMapping.length; i++) {
                String s = DataProvider.dataDictionaryEntries.get(selection.getStartLine() + rowIndex).get(i);
                if (s != null && !s.equals("")) {
                    sb.append("<b>");
                    sb.append(DataProvider.dataDictionaryHeaderMapping[i]);
                    sb.append("</b>");
                    sb.append("<br>");
                    for (String st : Utils.splitToolTipString(s)) {
                        sb.append(st);
                        sb.append("<br>");
                    }
                }
            }
            sb.setLength(sb.length() - 4); // get rid of last br
            sb.append("</html>");
            return sb.toString();
        } else {
            return null;
        }
    }

    private void doDelete() {
        int col = table.getSelectedColumn();
        int row = table.getSelectedRow();
        table.getModel().setValueAt("", row, col);
        repaint();
    }

    private void doCopy() {
        int col = table.getSelectedColumn();
        int row = table.getSelectedRow();
        if (col != -1 && row != -1) {
            Object value = table.getValueAt(row, col);

            String data;
            if (value == null) {
                data = "";
            } else {
                data = value.toString();
            }

            final StringSelection selection = new StringSelection(data);

            final Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(selection, selection);
        }
    }

    private void doPaste() {
        final Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        final Transferable content = clipboard.getContents(this);
        if (content != null) {
            try {
                final String value = content.getTransferData(DataFlavor.stringFlavor).toString();
                table.getModel().setValueAt(value, lastSelectedRow, lastSelectedColumn);

                /*
                final int col = table.getSelectedColumn();
                final int row = table.getSelectedRow();
                if (table.isCellEditable(row, col)) {
                    table.setValueAt(value, row, col);
                    if (table.getEditingRow() == row && table.getEditingColumn() == col) {
                        final CellEditor editor = table.getCellEditor();
                        editor.cancelCellEditing();
                        table.editCellAt(row, col);
                    }
                }

                */
                table.repaint();
            } catch (UnsupportedFlavorException e) {
                // String have to be the standard flavor
                System.err.println("UNSUPPORTED FLAVOR EXCEPTION " + e.getLocalizedMessage());
            } catch (IOException e) {
                // The data is consumed?
                System.err.println("DATA CONSUMED EXCEPTION " + e.getLocalizedMessage());
            }
        }
    }

    private void initButton() {
        button = new JButton();
        button.setText(selection.toString());
        button.setBackground(Utils.getAssignedColor(selection.getNameSet()));
        //button.setAlignmentX(Component.CENTER_ALIGNMENT);
        //??????????????????????????????????
        button.setMaximumSize(new Dimension(100000, 25));
        button.setEnabled(true);
        button.setVisible(true);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                /*
                table.setVisible(!table.isVisible());
                table.getTableHeader().setVisible(!table.getTableHeader().isVisible());

                //remove(table);
                //remove(table.getTableHeader());
                SelectionTable.this.setPreferredSize(SelectionTable.this.getPreferredSize());
                SelectionTable.this.revalidate();
                SelectionTable.this.repaint();
                */
            }
        });
    }

    private void updateAnnotation(Annotation a, String fieldName, String newValue) {
        a.insertTuple(new Tuple(fieldName, newValue, annotationSet.getFieldNameSet()));
    }

    private class ToolTipHeader extends JTableHeader {

        public ToolTipHeader(TableColumnModel model) {
            super(model);
        }

        public String getToolTipText(MouseEvent e) {
            int col = columnAtPoint(e.getPoint());
            int modelCol = getTable().convertColumnIndexToModel(col);
            String retStr;
            try {
                retStr = annotationSet.getFieldNameSet().getFieldNames().get(modelCol - 2).getDescription();
                if (retStr == null || retStr.equals("")) {
                    retStr = null;
                } else {

                    StringBuilder sb = new StringBuilder();
                    sb.append("<html>");
                    for (String s : Utils.splitToolTipString(retStr)) {
                        sb.append(s);
                        sb.append("<br>");
                    }
                    sb.append("</html>");
                    retStr = sb.toString();


                }
            } catch (NullPointerException | ArrayIndexOutOfBoundsException ex) {
                retStr = "";
            }

            return retStr;
        }

    }

    private class CustomCellRenderer extends DefaultTableCellRenderer {

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

            Component rendererComp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            rendererComp.setForeground(Color.BLACK);

            if (lastSelectedTable && (row == lastSelectedRow || column == lastSelectedColumn)) {
                rendererComp.setBackground(new Color(183, 253, 255));
            } else {
                if (column == 0 || column == 1) {
                    rendererComp.setBackground(new Color(214, 225, 255));
                } else if (annotationSet.getFieldNameSet().getFieldNames().get(column - 2).isRequired()) {
                    rendererComp.setBackground(new Color(255, 252, 204));
                } else {
                    //if (row % 2 == 0) {
                    //    rendererComp.setBackground(new Color(220, 222, 251));
                    //} else {
                    rendererComp.setBackground(Color.WHITE);
                    //}

                }
            }


                return rendererComp ;
        }

    }


    private class TableModel extends AbstractTableModel {
        @Override
        public String getColumnName(int column) {
            if (column == 0) {
                return "Line";
            } else if (column == 1) {
                return "Field Name";
            }
            return annotationSet.getFieldNameSet().getFieldNames().get(column - 2).getName();
        }

        @Override
        public int getRowCount() {
            return annotationSet.getSize();
        }

        @Override
        public int getColumnCount() {
            return annotationSet.getFieldNameSet().getSize() + 2;
        }

        @Override
        public Object getValueAt(int i, int i1) {
            if (i1 == 0) {
                return selection.getStartLine() + i;
            } else if (i1 == 1) {

                if (DataProvider.dataDictionaryEntries != null) {
                    return DataProvider.dataDictionaryEntries.get(selection.getStartLine() + i).get(0);
                } else {
                    return "";
                }
            }
            Annotation a = annotationSet.getList().get(i);
            String field = annotationSet.getFieldNameSet().getFieldNames().get(i1 - 2).getName();
            return a.getTupleByLeft(field).getRight();
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return !(columnIndex == 0 || columnIndex == 1);
        }

        public void reconstruct() {
            fireTableStructureChanged();
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            super.setValueAt(aValue, rowIndex, columnIndex);

            Annotation a = annotationSet.getList().get(rowIndex);
            String fieldName = annotationSet.getFieldNameSet().getFieldNames().get(columnIndex - 2).getName();
            String newValue = (String) aValue;

            updateAnnotation(a, fieldName, newValue);
        }
    }

    public Selection getSelection() {
        return selection;
    }

    public AnnotationSet getAnnotationSet() {
        return annotationSet;
    }

    public int getLastSelectedRow() {
        return lastSelectedRow;
    }

    public int getLastSelectedColumn() {
        return lastSelectedColumn;
    }

    public boolean isLastSelectedTable() {
        return lastSelectedTable;
    }

    public void setLastSelectedTable(boolean lastSelectedTable) {
        this.lastSelectedTable = lastSelectedTable;
    }
}
