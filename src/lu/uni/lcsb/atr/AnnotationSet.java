package lu.uni.lcsb.atr;

import java.util.ArrayList;

/**
 * Created by sysadmin on 21.07.15.
 */
public class AnnotationSet {

    private ArrayList<Annotation> annotationList;
    private FieldNameSet fieldNameSet;

    public AnnotationSet(FieldNameSet fieldNameSet) {
        this.annotationList = new ArrayList<>();
        this.fieldNameSet = fieldNameSet;
    }

    public void changeNameSet(FieldNameSet nameSet) {
        fieldNameSet = nameSet;
        int oldSize = annotationList.size();
        annotationList = new ArrayList<>(oldSize);
        for (int i = 0; i < oldSize; i++) {
            annotationList.add(new Annotation(fieldNameSet));
        }
    }

    public int getSize() {
        return annotationList.size();
    }

    public void addAnnotation(Annotation a) {
        annotationList.add(a);
    }

    public void insertAnnotation(int index, Annotation a) {
        annotationList.add(index, a);
    }

    public ArrayList<Annotation> getList() {
        return annotationList;
    }

    public void deleteLast() {
        annotationList.remove(annotationList.get(annotationList.size()-1));
    }

    public FieldNameSet getFieldNameSet() {
        return fieldNameSet;
    }


}
