package lu.uni.lcsb.atr;

/**
 * Created by sysadmin on 04.08.15.
 */
public class SimpleTable {

    private SelectionTable selectionTable;

    SimpleTable(SelectionTable selectionTable) {
        this.selectionTable = selectionTable;
    }

    public String getDomain() {
        return selectionTable.getAnnotationSet().getFieldNameSet().getID();
    }

    public String[] getHeaders() {
        FieldNameSet set = selectionTable.getAnnotationSet().getFieldNameSet();
        String[] result = new String[set.getSize()];
        for (int i = 0; i < result.length; i++) {
            result[i] = set.getFieldNames().get(i).getName();
        }
        return result;
    }

    public String[][] getTable() {
        AnnotationSet set = selectionTable.getAnnotationSet();
        String[][] result = new String[set.getSize()][set.getFieldNameSet().getSize()];
        for (int i = 0; i < set.getSize(); i++) {
            for (int k = 0; k < set.getFieldNameSet().getSize(); k++) {
                result[i][k] = set.getList().get(i).getTuples().get(k).getRight();
            }
        }
        return result;
    }

    public int getColumnIndexByName(String columnName) {
        String[] header = getHeaders();
        for (int i = 0; i < header.length; i++) {
            if (header[i].equals(columnName)) {
                return i;
            }
        }
        return -1;
    }

    public boolean isRequired(int columnIndex) {
        return selectionTable.getAnnotationSet().getFieldNameSet().getFieldNames().get(columnIndex).isRequired();
    }

    public boolean isRequired(String columnName) {
        FieldName fn = selectionTable.getAnnotationSet().getFieldNameSet().getFieldNameByName(columnName);
        return fn != null && fn.isRequired();
    }

    public int getHeight() {
        return selectionTable.getAnnotationSet().getSize();
    }

    public int getWidth() {
        return selectionTable.getAnnotationSet().getFieldNameSet().getSize();
    }
}
