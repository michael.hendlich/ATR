package lu.uni.lcsb.atr;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by sysadmin on 21.07.15.
 */
public class Frame extends JFrame{
    private JPanel rootPanel;
    private JButton btnExportString;
    private JButton btnExportCSV;
    private JButton btnFillColumn;
    private JRadioButton radioFillSame;
    private JRadioButton radioFillNumerical;
    private JRadioButton radioFillMixed;
    private JComboBox comboBoxDomain;
    private JTextField txtFieldPrefix;
    private JButton btnChooseDomain;
    private JButton btnSaveProject;
    private JButton btnLoadProject;
    private JPanel scrollPanePanel;
    private JScrollPane scrollPane;
    private JRadioButton radioBtnToolGlobal;
    private JRadioButton radioBtnToolDomain;
    private JRadioButton radioBtnToolSingle;
    private JButton btnDDSettings;
    private JList listTools;
    private JButton btnAppllyTool;
    private JTextField txtFieldToolAdditionalInfo;
    private JTextArea txtAreaToolInfo;
    private JButton btnExportAPI;
    private JButton btnNewProject;
    private JButton btnApiSettings;
    private JPanel tablePanel;

    private CustomListModel listModel;

    private ToolMode toolMode;

    private FlowCode flowCode = FlowCode.nothing;

    public Frame() {
        super();
        ImageIcon img = new ImageIcon("res/logo4.png");
        setIconImage(img.getImage());
        setContentPane(rootPanel);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        createTitle();
        init();

        initBtnChooseDomain();
        initComboBox();
        initBtnExportString();
        initBtnSaveProject();
        initBtnExportCSV();
        initBtnLoadProject();
        initToolRadioButtons();
        initBtnDDSettings();
        initListTools();
        initBtnApplyTool();
        initBtnNewProject();
        initBtnExportAPI();
        initBtnAPISettings();

        loadTables();

        SharedFunctionality.executeToolsOnLoad();

        pack();
        setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);

    }

    private void createTitle() {
        StringBuilder sb = new StringBuilder();
        sb.append(DataProvider.project.getName());
        sb.append("  -  ");
        sb.append(DataProvider.project.getSaveFile().getAbsolutePath());
        setTitle(sb.toString());
    }

    private void initBtnAPISettings() {
        btnApiSettings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                APIDialog dialog = new APIDialog(null);
                dialog.setVisible(true);
            }
        });
    }

    private void initBtnNewProject() {
        btnNewProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int i = JOptionPane.showConfirmDialog(
                        Frame.this,
                        "You will lose all unsaved data. Continue?",
                        "Warning",
                        JOptionPane.YES_NO_OPTION);
                if (i == JOptionPane.OK_OPTION) {
                    flowCode = FlowCode.newProject;
                }

            }
        });
    }

    private void initBtnApplyTool() {
        btnAppllyTool.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String selectedTool = (String) listTools.getSelectedValue();
                SharedFunctionality.applyTool(selectedTool, toolMode, txtFieldToolAdditionalInfo.getText());
            }
        });
    }

    private void initListTools() {
        listModel = new CustomListModel();
        listTools.setModel(listModel);
        listTools.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listTools.setLayoutOrientation(JList.VERTICAL);

        ToolTipManager.sharedInstance().setInitialDelay(0);

        listTools.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent evt) {
                if (!evt.getValueIsAdjusting()) {
                    txtAreaToolInfo.setText(Tools.getTools().get(evt.getFirstIndex()).getAdditionalInfoHelp());
                }
            }


        });
        listTools.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseDragged(MouseEvent e) {
                // no-op
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                JList l = (JList) e.getSource();
                int index = l.locationToIndex(e.getPoint());
                if (index > -1) {
                    l.setToolTipText(Tools.getTools().get(index).getDescription());
                }
            }
        });
    }

    private void initBtnDDSettings() {
        btnDDSettings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DDSettingsDialog dialog = new DDSettingsDialog(null);
                dialog.setVisible(true);
            }
        });
    }

    private void initToolRadioButtons() {

        // default
        toolMode = ToolMode.single;

        radioBtnToolGlobal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                toolMode = ToolMode.global;
            }
        });

        radioBtnToolDomain.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                toolMode = ToolMode.domain;
            }
        });

        radioBtnToolSingle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                toolMode = ToolMode.single;
            }
        });
    }

    private void loadTables() {
        for (SelectionTable table : DataProvider.project.getTables()) {
            tablePanel.add(table);
        }
    }

    private void createUIComponents() {
        tablePanel = new JPanel(true);
        //tablePanel.setLayout(new java.awt.GridLayout(0, 1));
        BoxLayout layout = new BoxLayout(tablePanel, BoxLayout.PAGE_AXIS);
        tablePanel.setLayout(layout);
        tablePanel.setVisible(true);
        scrollPane = new JScrollPane(tablePanel);
    }

    private void updateAllTables() {
        for (SelectionTable st : DataProvider.project.getTables()) {
            st.update();
        }
        revalidate();
        repaint();
    }

    private void initBtnLoadProject() {
        btnLoadProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                LoadProjectDialog dialog = new LoadProjectDialog(null);
                dialog.setVisible(true);
                reloadProject();
                createTitle();
                revalidate();
                pack();
                repaint();
            }
        });
    }

    private void reloadProject() {
        tablePanel.removeAll();

        for (SelectionTable table : DataProvider.project.getTables()) {
            tablePanel.add(table);
        }

        revalidate();
        repaint();
    }

    private void initBtnExportAPI() {
        btnExportAPI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (DataProvider.password == null) {
                    JOptionPane.showMessageDialog(Frame.this, "No REDCap password defined");
                } else {
                    SharedFunctionality.createModifiedDD(DataProvider.project.getTables());
                    boolean worked = SharedFunctionality.uploadDD();
                    if (!worked) {
                        JOptionPane.showMessageDialog(Frame.this, "Upload error");
                    }
                }

            }
        });
    }

    private void initBtnExportCSV() {
        btnExportCSV.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                ArrayList<ArrayList<String>> temp = FileIO.readCSVFile(DataProvider.dataDictionary);
                boolean overwritten = false;

                for (SelectionTable table : DataProvider.project.getTables()) {

                    ArrayList<String> ins = new ArrayList<>();
                    for (Annotation a : table.getAnnotationSet().getList()) {
                        ins.add(a.toFormattedString());
                    }

                    boolean res = FileIO.modifyLoadedCSVFile(temp, ins, table.getSelection().getStartLine());
                    if (res) {
                        overwritten = true;
                    }
                }
                if (!DataProvider.alwaysOverwrite && overwritten) {
                    int result = JOptionPane.showConfirmDialog(Frame.this, "There are existing entries, overwrite?", "Existing entries", JOptionPane.YES_NO_OPTION);
                    if (result == JOptionPane.YES_OPTION) {
                        FileIO.writeCSVFile(temp, DataProvider.dataDictionary);
                        DataProvider.dataDictionaryEntries = FileIO.readCSVFile(DataProvider.dataDictionary);
                    }
                } else {
                    FileIO.writeCSVFile(temp, DataProvider.dataDictionary);
                    DataProvider.dataDictionaryEntries = FileIO.readCSVFile(DataProvider.dataDictionary);
                }

            }
        });
    }

    private void initBtnSaveProject() {
        btnSaveProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fileChooser = new JFileChooser() {
                    @Override
                    public void approveSelection() {
                        File f = getSelectedFile();
                        if (f.exists() && getDialogType() == SAVE_DIALOG) {
                            int result = JOptionPane.showConfirmDialog(this, "The file already exists, overwrite?", "Existing file", JOptionPane.YES_NO_CANCEL_OPTION);
                            switch (result) {
                                case JOptionPane.YES_OPTION:
                                    super.approveSelection();
                                    return;
                                case JOptionPane.NO_OPTION:
                                    return;
                                case JOptionPane.CLOSED_OPTION:
                                    return;
                                case JOptionPane.CANCEL_OPTION:
                                    cancelSelection();
                                    return;
                            }
                        }
                        super.approveSelection();
                    }
                };
                fileChooser.setSelectedFile(DataProvider.project.getSaveFile());
                if (fileChooser.showSaveDialog(Frame.this) == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();

                    String fileName = file.getAbsolutePath();
                    if (!fileName.endsWith(DataProvider.DEFAULT_METADATA_FILE_EXTENSION)) {
                        file = new File(fileName + DataProvider.DEFAULT_METADATA_FILE_EXTENSION);
                    }
                    Frame.this.setTitle(file.getName());
                    SharedFunctionality.saveProject(file);
                }
            }
        });
    }


    /*
    private String intToFormattedString(int number) {
        StringBuilder sb = new StringBuilder(Integer.toString(number));
        int numberLength = Integer.toString(annotationSet.getSize()).length();
        int zeroesToAdd = numberLength - sb.length();
        for (int i = 0; i < zeroesToAdd; i++) {
            sb.insert(0,"0");
        }

        return sb.toString();
    }
    */

    private void initBtnExportString() {
        if (btnExportString != null) {
            btnExportString.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                /*
                StringBuilder sb = new StringBuilder();
                for (Annotation a : annotationSet.getList()) {
                    System.out.println(a.toFormattedString());
                    sb.append(a.toFormattedString());
                    sb.append(System.lineSeparator());
                }
                String out = sb.toString();

                StringSelection selection = new StringSelection(out);
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection, selection);
                */
                }

            });
        }

    }


    private void initBtnChooseDomain() {
        btnChooseDomain.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int result = JOptionPane.showConfirmDialog(Frame.this, "This will delete your current table. Continue?", "Warning", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    FieldNameSet newNameSet = Utils.getNameSetByID(comboBoxDomain.getSelectedItem().toString());
                    SelectionTable selectedTable = Utils.getSelectedTable();
                    if (selectedTable != null) {
                        SharedFunctionality.changeDomain(selectedTable, newNameSet);
                    }
                    updateAllTables();
                }
            }
        });

    }

    public FlowCode getFlowCode() {
        return flowCode;
    }

    public void setFlowCode(FlowCode flowCode) {
        this.flowCode = flowCode;
    }

    private void initComboBox() {
        comboBoxDomain.removeAllItems();
        for (FieldNameSet set : DataProvider.loadedNameSets) {
            comboBoxDomain.addItem(set.getID());
        }
    }

    private void init() {
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                flowCode = FlowCode.exit;
                super.windowClosing(e);
            }
        });

        /*
        if (DataProvider.dataDictionaryEntries == null) {
            if (DataProvider.dataDictionary == null) {
                APIConnector.downloadDD();
                DataProvider.dataDictionary = Utils.getNewestInFile();
            }
            DataProvider.dataDictionaryEntries = FileIO.readCSVFile(DataProvider.dataDictionary);
        }
        */
    }

    private class CustomListModel extends AbstractListModel {

        @Override
        public int getSize() {
            return Tools.getTools().size();
        }

        @Override
        public Object getElementAt(int i) {
            return Tools.getTools().get(i).getName();
        }

        public void update() {
            fireContentsChanged(this,0,getSize()-1);
        }

    }

}
