package lu.uni.lcsb.atr;

import java.util.ArrayList;

/**
 * Created by sysadmin on 21.07.15.
 */
public class Annotation {

    private ArrayList<Tuple> tuples;
    private FieldNameSet nameSet;

    public Annotation(FieldNameSet nameSet) {
        tuples = new ArrayList<>();
        this.nameSet = nameSet;
        for (FieldName fn : nameSet.getFieldNames()) {
            tuples.add(new Tuple(fn.getName(), null, nameSet));
        }
    }

    public void insertTuple(Tuple tuple) {
        getTupleByLeft(tuple.getLeft()).setRight(tuple.getRight());
    }

    public void updateTuple(int index, String newRight) {
        tuples.get(index).setRight(newRight);
    }

    public Tuple getTupleByLeft(String left) {
        for (Tuple t : tuples) {
            if (t.getLeft().equals(left)) {
                return t;
            }
        }
        return null;

    }

    public String toFormattedString() {
        StringBuilder sb = new StringBuilder();
        for (Tuple t : tuples) {
            if (t.getRight() != null && !t.getRight().equals("")) {
                sb.append(t.getLeft());
                sb.append("=");
                if (!t.getRight().contains("[")) {
                    sb.append("\"");
                }
                sb.append(t.getRight());
                if (!t.getRight().contains("[")) {
                    sb.append("\"");
                }
                sb.append(";");
            }
        }
        return sb.toString();
    }

    public String toCSVLine() {
        StringBuilder sb = new StringBuilder();
        for (Tuple t: tuples) {
            if (t.getRight() != null) {
                sb.append(t.getRight());
            }
            sb.append(",");
        }
        return sb.toString();
    }

    public ArrayList<Tuple> getTuples() {
        return tuples;
    }

    public boolean isEmpty() {
        return tuples.isEmpty();
    }

}
