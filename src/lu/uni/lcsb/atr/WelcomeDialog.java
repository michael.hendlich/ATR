package lu.uni.lcsb.atr;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class WelcomeDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel panelImg;
    private JButton btnLoadProject;
    private JPanel panelLogo;

    private FlowCode flowCode;

    public WelcomeDialog(Window window) {
        super(window);
        setContentPane(contentPane);
        setModalityType(ModalityType.APPLICATION_MODAL);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Annotation Tool for REDCap");
        setResizable(true);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        initBtnLoad();

        pack();
        setLocationRelativeTo(null);
    }

    private void initBtnLoad() {
        btnLoadProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                //Utils.restartApplication();
                flowCode = FlowCode.loadProject;
                dispose();
            }
        });
    }

    private void onOK() {
        flowCode = FlowCode.newProject;
        dispose();
    }

    private void onCancel() {
        flowCode = FlowCode.exit;
        dispose();
    }

    private void createUIComponents() {
        panelImg = new JPanel(true) {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

                File file = new File("res/logo6_small.png");
                BufferedImage image = null;
                try {
                    image = ImageIO.read(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (image != null) {
                    g.drawImage(image, 0, -50, this);
                }
            }
        };
        panelLogo = new JPanel(true) {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                File file = new File("res/lcsb_logo.png");
                BufferedImage image = null;
                try {
                    image = ImageIO.read(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if(image != null){
                    g.drawImage(image, 0, 0, this);
                }
            }
        };
    }

    public FlowCode getFlowCode() {
        return flowCode;
    }
}
