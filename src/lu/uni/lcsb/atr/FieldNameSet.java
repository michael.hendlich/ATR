package lu.uni.lcsb.atr;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sysadmin on 21.07.15.
 */
public class FieldNameSet {

    //DOMAIN,RDOMAIN,QSCAT,QSSCAT,QSTEST,QSTESTCD,QSEVAL,QSEVLINT,QSORRES,QSSTRESC,QSSTRESN,QSGRPID,SUPPQS.IDVAR,SUPPQS.IDVARVAL,SUPPQS.QNAM,SUPPQS.QLABEL,SUPPQS.QVAL

    private String ID;
    private ArrayList<FieldName> fieldNames;

    public FieldNameSet(ArrayList<FieldName> fieldNames, String ID) {
        this.ID = ID;
        this.fieldNames = fieldNames;
    }


    public ArrayList<FieldName> getFieldNames() {
        return fieldNames;
    }

    public FieldName getFieldNameByName(String name) {
        for (FieldName fn : fieldNames) {
            if (fn.getName().equals(name)) {
                return fn;
            }
        }
        return null;
    }

    public int getSortPosition(String name) {
        for (FieldName f : fieldNames) {
            if (f.getName().equals(name)) {
                return fieldNames.indexOf(f);
            }
        }
        return -1;
    }

    public boolean hasEqualNames(List<String> other) {
        if (other.size() != fieldNames.size()) {
            return false;
        }
        for (int i = 0; i < other.size(); i++) {
            if (!other.get(i).equals(fieldNames.get(i).getName())) {
                return false;
            }
        }
        return true;
    }

    /*
    public String toCSVLine() {
        StringBuilder sb = new StringBuilder();
        for (String s : fieldNames) {
            sb.append(s);
            sb.append(",");
        }
        return sb.toString();
    }
    */

    public int getSize() {
        return fieldNames.size();
    }

    public String getID() {
        return ID;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FieldNameSet{");
        sb.append("ID='").append(ID).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
