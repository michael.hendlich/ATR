package lu.uni.lcsb.atr;

import java.util.ArrayList;

/**
 * Created by sysadmin on 04.08.15.
 */
public interface ATRTool {
    ArrayList<String[][]> onToolUse(ArrayList<SimpleTable> tables, ToolMode toolMode, int lastSelectedRowIndex, int lastSelectedColumnIndex, int lastSelectedTableIndex, String additionalInfo);
    ArrayList<String[][]> onLoad(ArrayList<SimpleTable> tables);

    String getName();
    String getDescription();
    String getAdditionalInfoHelp();
}
