package lu.uni.lcsb.atr.tools;

import lu.uni.lcsb.atr.ATRTool;
import lu.uni.lcsb.atr.SimpleTable;
import lu.uni.lcsb.atr.ToolMode;

import java.util.ArrayList;

/**
 * Created by sysadmin on 10.08.15.
 */
public class EmptyTool implements ATRTool {
    @Override
    public ArrayList<String[][]> onToolUse(ArrayList<SimpleTable> tables, ToolMode toolMode, int lastSelectedRowIndex, int lastSelectedColumnIndex, int lastSelectedTableIndex, String additionalInfo) {
        return null;
    }

    @Override
    public ArrayList<String[][]> onLoad(ArrayList<SimpleTable> tables) {
        return null;
    }

    @Override
    public String getName() {
        return "Empty test tool";
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public String getAdditionalInfoHelp() {
        return "does nothing";
    }
}
