package lu.uni.lcsb.atr.tools;

import lu.uni.lcsb.atr.ATRTool;
import lu.uni.lcsb.atr.SimpleTable;
import lu.uni.lcsb.atr.ToolMode;

import java.util.ArrayList;

/**
 * Created by sysadmin on 10.08.15.
 */
public class AutoDomainTool implements ATRTool {
    @Override
    public ArrayList<String[][]> onToolUse(ArrayList<SimpleTable> tables, ToolMode toolMode, int lastSelectedRowIndex, int lastSelectedColumnIndex, int lastSelectedTableIndex, String additionalInfo) {
        return null;
    }

    @Override
    public ArrayList<String[][]> onLoad(ArrayList<SimpleTable> tables) {
        ArrayList<String[][]> resultList = new ArrayList<>();
        for (SimpleTable table : tables) {
            String[][] newTable = table.getTable();

            int columnIndex = table.getColumnIndexByName("DOMAIN");
            if (columnIndex >= 0) {
                for (String[] row : newTable) {
                    row[columnIndex] = table.getDomain();
                }
            }

            resultList.add(newTable);
        }
        return resultList;
    }

    @Override
    public String getName() {
        return "AutoDomainTool";
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public String getAdditionalInfoHelp() {
        return "This tool will automatically fill in the DM column after creating a table";
    }
}
