----------------------------
|   CUSTOM TOOLS FOR ATR   |
----------------------------

Custom tools enable you to write java code which can modify your annotation table data. This is done using a simple API.

Those tools will then either be executed on your data when
    a) loading them, and/or
    b) when the user applies a tool on one or more selected tables, using the provided GUI.

When invoked bt the user (case b), can provide additional information to the tool:
    1. The Tool Mode (global, domain-specific or single) - see type explanation.
    2. An additional execution parameter string, which can be used to further specify the tool behavior.

The custom tools show up in the "tools list" at the top right of the table editor. The creator can provide additional help text for the tool,
to explain the behavior of the tool, and the syntax of the additional execution parameter string (if needed).

See the commentated ColumnFillTool.java tool as an example on how to write a simple tool.


How to add custom Tools to the ATR Program:

1. Create a class in this package that implements "ATRTool".
2. Implement the interface methods:

    String getName():
        Return the name of your tool.

    String getDescription():
        Return the description text of your tool (optional, return "" if not needed).

    String getAdditionalInfoHelp():
        Return the help text for the additional information text field (optional, return "" if not needed).

    ArrayList<String[][]> onLoad(ArrayList<SimpleTable> tables);
        This method will be called on all tables when loading into the program, before showing them to the user.
        Return modified tables in format [row][column] and in the same order, or return null if you don't want to change the table.

    public ArrayList<String[][]> onToolUse(ArrayList<SimpleTable> tables, ToolMode toolMode, int lastSelectedRowIndex, int lastSelectedColumnIndex, int lastSelectedTableIndex):
        This method will be called when the user starts a specific tool with a specific tool mode.
        The indexes specify the last cell the user has selected before using the tool. The selected table is at position lastSelectedTableIndex in the tables list (-1 if none selected).
        AdditionalInfo is the text the user wrote into the provided text field, to be able to further specify the behavior of the tool.
        Return modified table(s) in format [row][column] and in the same order, or return null if you don't want to change the table.


3. Register your tool in the ToolRegistry class in this package.
4. Recompile and run ATR. The tool should now show up in the "Tools" list.


Type explanations:

    public enum ToolMode {single, domain, global} :
        Specifies the scope of the tool application and the attached SimpleTables
           single : only the user-selected table
           domain: all tables using the same domain as the user-selected table
           global : all domains


    public class SimpleTable {

      Simple Object wrapping the table containing annotation data. It has the same format as shown to the user.

      public String getDomain():
            Returns the name of the domain used in this table.

      public String[] getHeaders():
          Returns the table column header names.

      public String[][] getTable():
          Returns the table cells in [row] [column] format.

      public int getColumnIndexByName(String columnName) :
          Returns the index of the given column header name.

      public boolean isRequired(int columnIndex):
           Returns true if the specified column is marked as required, otherwise returns false.

      public boolean isRequired(String columnName):
           Returns true if the specified column is marked as required, otherwise returns false.

      public int getHeight():
           Returns the table height / number of rows.

      public int getWidth():
           Returns the table width / number of columns.

    }

