package lu.uni.lcsb.atr.tools;

import lu.uni.lcsb.atr.ATRTool;
import lu.uni.lcsb.atr.SimpleTable;
import lu.uni.lcsb.atr.ToolMode;

import java.util.ArrayList;

/**
 * Created by sysadmin on 10.08.15.
 */
public class ShortcutTool implements ATRTool {
    @Override
    public ArrayList<String[][]> onToolUse(ArrayList<SimpleTable> tables, ToolMode toolMode, int lastSelectedRowIndex, int lastSelectedColumnIndex, int lastSelectedTableIndex, String additionalInfo) {

        ArrayList<String[][]> resultList = new ArrayList<>();

        // iterate through tables
        for (int i = 0; i < tables.size(); i++) {
            String[][] newTable = tables.get(i).getTable();

            // Check if the current table fits the ToolMode criteria.
            if ((toolMode == ToolMode.global) ||
                    (toolMode == ToolMode.single && i == lastSelectedTableIndex) ||
                    (toolMode == ToolMode.domain && tables.get(i).getDomain().equals(tables.get(lastSelectedTableIndex).getDomain()))) {

                for (int k = 0; k < newTable.length; k ++) {
                    for (int l = 0; l < newTable[0].length; l++) {
                        String s = newTable[k][l];
                        String newString = s;
                        if (s != null && (s.equals("int") || s.equals("percent") || s.equals("text") || s.equals("number") || s.equals("time") || s.equals("datetime"))) {
                            newString = "[:" + s + ":]";
                        }
                        newTable[k][l] = newString;
                    }
                }
            }

            resultList.add(newTable);
        }

        return resultList;

    }

    @Override
    public ArrayList<String[][]> onLoad(ArrayList<SimpleTable> tables) {
        return null;
    }

    @Override
    public String getName() {
        return "Shortcuts";
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getAdditionalInfoHelp() {
        return "This tool allows you to use short forms of variable placeholders (e.g. int instead of  [:int:]) Applying this tool will then convert them to their long form. \n" +
                "Supported: int, percent, text, number, time, datetime";
    }
}
