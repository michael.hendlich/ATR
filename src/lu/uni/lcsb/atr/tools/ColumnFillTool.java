package lu.uni.lcsb.atr.tools;

import lu.uni.lcsb.atr.ATRTool;
import lu.uni.lcsb.atr.SimpleTable;
import lu.uni.lcsb.atr.ToolMode;

import java.util.ArrayList;
/**
 * Created by sysadmin on 04.08.15.
 */
public class ColumnFillTool implements ATRTool {

    /*
    This method provides a way to quickly fill out whole columns intelligently (like in Excel).
    It will only be applied to te tables which fit the ToolMode.
    It will automatically choose between 3 modes : fill same, fill numerical or fill mixed.
    */
    @Override
    public ArrayList<String[][]> onToolUse(ArrayList<SimpleTable> tables, ToolMode toolMode, int lastSelectedRowIndex, int lastSelectedColumnIndex, int lastSelectedTableIndex, String additionalInfo) {

        // only apply tool if a selection was made
        if (lastSelectedTableIndex >= 0 && lastSelectedColumnIndex >= 0 && lastSelectedRowIndex >= 0) {

            // For filling out whole columns, we use the value of the cell in the first row to determine how to fill the other cells.
            String selectedCellValue = tables.get(lastSelectedTableIndex).getTable()[0][lastSelectedColumnIndex];

            ArrayList<String[][]> resultList = new ArrayList<>();

            // get the header name of the selected column
            String columnName = tables.get(lastSelectedTableIndex).getHeaders()[lastSelectedColumnIndex];

            // iterate through tables
            for (int i = 0; i < tables.size(); i++) {
                String[][] newTable = tables.get(i).getTable();

                // Check if the current table fits the ToolMode criteria.
                if ((toolMode == ToolMode.global) ||
                        (toolMode == ToolMode.single &&  i== lastSelectedTableIndex) ||
                        (toolMode == ToolMode.domain && tables.get(i).getDomain().equals(tables.get(lastSelectedTableIndex).getDomain()))) {

                    // check if the selected column also exists in this table (relevant for global tool mode)
                    int localSelectedColumnIndex = tables.get(i).getColumnIndexByName(columnName);
                    if (localSelectedColumnIndex >= 0) {

                         /*
                         Mode : fill same
                         If the first cell has some text in it, and the additional text field is empty, fill all cells of the column with the same text as the first cell.
                         */
                        if (selectedCellValue != null && !selectedCellValue.equals("") &&
                                (additionalInfo == null || additionalInfo.equals(""))) {

                            for (String[] row : newTable) {
                                row[localSelectedColumnIndex] = selectedCellValue;
                            }

                        /*
                         Mode : numerical
                         If both the first cell, and the additional text field are empty, fill the cells counting up, starting at 1.
                         */
                        } else if ((selectedCellValue == null || selectedCellValue.equals("")) &&
                                (additionalInfo == null || additionalInfo.equals(""))) {

                            for (int k = 0; k < newTable.length; k++) {
                                newTable[k][localSelectedColumnIndex] = String.valueOf(k+1);
                            }

                         /*
                         Mode : mixed
                         If the first cell is empty, but the additional text field contains text, use the additional text as a prefix,
                          and then append numbers counting up, starting at 1
                         */
                        } else if ((selectedCellValue == null || selectedCellValue.equals("")) &&
                                (additionalInfo != null && !additionalInfo.equals(""))) {

                            for (int k = 0; k < newTable.length; k++) {
                                newTable[k][localSelectedColumnIndex] = additionalInfo + (k + 1);
                            }
                        }
                    }
                }

                resultList.add(newTable);
            }

            return resultList;

        } else {
            // if no selection was made, change nothing
            return null;
        }

    }

    // When the program is started, all cells of the first column in all tables, regardless of domain, will be filled with "Hello!".
    @Override
    public ArrayList<String[][]> onLoad(ArrayList<SimpleTable> tables) {
        /*
        ArrayList<String[][]> resultList = new ArrayList<>();
        for (SimpleTable table : tables) {
            String[][] newTable = table.getTable();
            for (int i = 0; i < table.getHeight(); i++) {
                newTable[i][0] = "Hello!";
            }
            resultList.add(newTable);
        }
        return resultList;
        */
        return null;
    }

    // Returns the tool name, which will be show in the tools list.
    @Override
    public String getName() {
        return "Fill Column";
    }

    // Returns the tool description, which will be shown in a tooltip when hovering the cursor over the tool entry in the tools list.
    @Override
    public String getDescription() {
        return "This is the tooltip!";
    }

    // Returns the additional help text, which will be shown in the additional text field on the right.
    @Override
    public String getAdditionalInfoHelp() {
        StringBuilder sb = new StringBuilder();
        sb.append("This tool will fill all cells in the last selected column ");
        sb.append("\n");
        sb.append("It will use one of three modes, depending on the context : ");
        sb.append("\n");
        sb.append("A : If the first cell of the column has some text in it, and the parameter text  field is empty, fill all cells of the column with the same text as the first cell.");
        sb.append("\n");
        sb.append("B : If both the first cell, and the additional text field are empty, fill the cells - counting up, starting at 1.");
        sb.append("\n");
        sb.append("C : If the first cell is empty, but the additional text field contains text, use the additional text as a prefix, and then append numbers counting up, starting at 1");
        return sb.toString();
    }
}
