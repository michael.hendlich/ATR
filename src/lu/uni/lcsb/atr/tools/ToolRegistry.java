package lu.uni.lcsb.atr.tools;

import lu.uni.lcsb.atr.ATRTool;

import java.util.ArrayList;

/**
 * Created by sysadmin on 04.08.15.
 */
public class ToolRegistry {

    public static ArrayList<ATRTool> tools = new ArrayList<>();

    public static void addYourToolsHere() {
        // ex: tools.add(new MyTool);
        tools.add(new ColumnFillTool());
        tools.add(new ShortcutTool());
        tools.add(new EmptyTool());
        tools.add(new AutoDomainTool());
    }

}
