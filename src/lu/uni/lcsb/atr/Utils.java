package lu.uni.lcsb.atr;

import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import java.io.File;
import java.io.FileFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * Created by sysadmin on 29.07.15.
 */
public class Utils {

    public static boolean domainAlreadyLoaded(String name) {
        for (FieldNameSet set : DataProvider.loadedNameSets) {
            if (set.getID().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public static SelectionTable findSelectionTableBySelection(Selection selection) {
        for (SelectionTable table : DataProvider.project.getTables()) {
            Selection s = table.getSelection();
            if (s.getStartLine() == selection.getStartLine() || s.getEndLine() == selection.getEndLine()) {
                return table;
            }
        }
        return null;
    }

    public static ArrayList<String> splitToolTipString(String s) {
        ArrayList<String> result = new ArrayList<>();

        String [] tokens = StringUtils.split(s);

        int i = 0;
        StringBuilder line = new StringBuilder();
        while (i < tokens.length) {
            if (line.length() + tokens[i].length() > 50) {
                result.add(line.toString());

                line = new StringBuilder();
                line.append(tokens[i]);
                line.append(" ");
            } else {
                line.append(tokens[i]);
                line.append(" ");
            }
            i++;
        }
        result.add(line.toString());


        return result;
    }

    public static void restartApplication() {
        try {
            final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
            final File currentJar = new File(UIFlow.class.getProtectionDomain().getCodeSource().getLocation().toURI());

        /* is it a jar file? */
            if(!currentJar.getName().endsWith(".jar"))
                return;

        /* Build command: java -jar application.jar */
            final ArrayList<String> command = new ArrayList<>();
            command.add(javaBin);
            command.add("-jar");
            command.add(currentJar.getPath());

            final ProcessBuilder builder = new ProcessBuilder(command);
            builder.start();
            System.exit(0);
        } catch (Exception e) {
            System.err.println("help");
        }

    }

    public static File getNewestInFile() {
        File fl = new File("backups/in/");
        File[] files = fl.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isFile();
            }
        });
        long lastMod = Long.MIN_VALUE;
        File choice = null;
        for (File file : files) {
            if (file.lastModified() > lastMod) {
                choice = file;
                lastMod = file.lastModified();
            }
        }
        return choice;
    }

    public static String getTimestamp() {
        return new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
    }

    public static SelectionTable getSelectedTable() {
        for (SelectionTable table : DataProvider.project.getTables()) {
            if (table.isLastSelectedTable()) {
                return table;
            }
        }
        return null;
    }

    public static void resetAllTableSelections() {
        for (SelectionTable table : DataProvider.project.getTables()) {
            table.setLastSelectedTable(false);
        }
    }

    public static Color randColor() {
        Color c;
        int r, g, b;
        do {
            r = randInt(50, 255);
            g = randInt(50, 255);
            b = randInt(50, 255);
            c = new Color(r, g, b);
        } while (r + g + b < 500 && r + g + b > 200);

        return c;
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    public static String stripFileExtension (String fileName) {
        if (fileName == null) {
            return null;
        }

        int pos = fileName.lastIndexOf(".");
        if (pos == -1) {
            return fileName;
        }
        return fileName.substring(0, pos);
    }

    public static void importDomainFile() {
        FileIO.importNewNameSetFile();
    }

    public static ArrayList<AnnotationSet> getAllAnnotationSets(ArrayList<SelectionTable> selectionTables) {
        ArrayList<AnnotationSet> result = new ArrayList<>();
        for (SelectionTable st : selectionTables) {
            result.add(st.getAnnotationSet());
        }
        return result;
    }

    public static AnnotationSet selectionToAnnotationSet(ArrayList<ArrayList<String>> dataDictionary, Selection selection) {
        AnnotationSet result = new AnnotationSet(selection.getNameSet());

        for (int i = 0; i < selection.getSize(); i++) {
            Annotation a = new Annotation(selection.getNameSet());
            result.addAnnotation(a);
        }

        return result;
    }

    public static FieldNameSet getNameSetByID(String id) {
        for (FieldNameSet set : DataProvider.loadedNameSets) {
            if (id.equals(set.getID())) {
                return set;
            }
        }
        return null;
    }

    public static boolean itemAlreadySelected(ArrayList<Selection> selections, int[] selectedItems) {
        for (Selection s : selections) {
            for (int selectedItem : selectedItems) {
                if (selectedItem >= s.getStartLine() && selectedItem <= s.getEndLine()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Selection getSelectionOfRow(ArrayList<Selection> selections, int row) {
        for (Selection s : selections) {
            if (row >= s.getStartLine() && row <= s.getEndLine()) {
                return s;
            }
        }
        return null;
    }

    public static Color getAssignedColor(FieldNameSet set) {
        if (hasColorAssigned(set)) {
            return DataProvider.colorAssignments.get(set);
        } else {
            if (DataProvider.colorAssignments.size() >= DataProvider.COLOR_PALETTE.length) {
                DataProvider.colorAssignments.put(set, randColor());
            } else {
                DataProvider.colorAssignments.put(set, DataProvider.COLOR_PALETTE[DataProvider.colorAssignments.size()]);
            }
            return DataProvider.colorAssignments.get(set);
        }
    }

    private static boolean hasColorAssigned(FieldNameSet set) {
        for (FieldNameSet assignedSet : DataProvider.colorAssignments.keySet()) {
            if (assignedSet != null && assignedSet.getID().equals(set.getID())) {
                return true;
            }
        }
        return false;
    }

    public static Color toDarkerColor(Color c) {
        int r = c.getRed() - 70;
        int g = c.getGreen() - 70;
        int b = c.getBlue() - 70;

        if (r < 0) {
            r = 0;
        }
        if (g < 0) {
            g = 0;
        }
        if (b < 0) {
            b = 0;
        }

        return new Color(r, g, b);

    }
}
