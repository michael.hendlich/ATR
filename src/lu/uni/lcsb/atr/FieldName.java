package lu.uni.lcsb.atr;

/**
 * Created by sysadmin on 19.08.15.
 */
public class FieldName {

    private String name;
    private boolean required;
    private String description;

    public FieldName(String name, boolean required, String description) {
        this.name = name;
        this.required = required;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
