package lu.uni.lcsb.atr;

import lu.uni.lcsb.atr.tools.ToolRegistry;

import java.util.ArrayList;

/**
 * Created by sysadmin on 04.08.15.
 */
public class Tools {

    private static ArrayList<ATRTool> tools = new ArrayList<>();

    public static void registerTools() {
        ToolRegistry.addYourToolsHere();
        for (ATRTool tool : ToolRegistry.tools) {
            tools.add(tool);
        }
    }

    public static ATRTool findToolbyName(String toolName) {
        for (ATRTool tool : tools) {
            if (tool.getName().equals(toolName)) {
                return tool;
            }
        }
        return null;
    }

    public static void onLoad() {
        ArrayList<SimpleTable> tables = new ArrayList<>();
        for (SelectionTable selectionTable : DataProvider.project.getTables()) {
            tables.add(new SimpleTable(selectionTable));
        }

        for (ATRTool tool : tools) {
            ArrayList<String[][]> results =  tool.onLoad(tables);
            if (results != null) {
                for (int i = 0; i < results.size(); i++) {
                    if (results.get(i) != null) {
                        DataProvider.project.getTables().get(i).modifyData(results.get(i));
                    }
                }
                System.out.println("Applied Tool : " + tool.getName());
            }
        }
    }

    public static void onToolUse(String toolName, ToolMode toolMode, String additionalInfo) {
        ATRTool tool = findToolbyName(toolName);

        if (tool != null) {
            ArrayList<SimpleTable> simpleTables = new ArrayList<>();

            int selectedRow = -1;
            int selectedColumn = -1;
            int selectedTable = -1;

            ArrayList<SelectionTable> selectionTables = DataProvider.project.getTables();
            for (int i = 0; i < selectionTables.size(); i++) {
                simpleTables.add(new SimpleTable(selectionTables.get(i)));
                if (selectionTables.get(i).isLastSelectedTable()) {
                    selectedTable = i;
                    selectedRow = selectionTables.get(i).getLastSelectedRow();
                    selectedColumn = selectionTables.get(i).getLastSelectedColumn() - 2;
                }
            }

            ArrayList<String[][]> results = tool.onToolUse(simpleTables, toolMode, selectedRow, selectedColumn, selectedTable, additionalInfo);
            if (results != null) {
                for (int i = 0; i < results.size(); i++) {
                    if (results.get(i) != null) {
                        DataProvider.project.getTables().get(i).modifyData(results.get(i));
                    }
                }
            }
            System.out.println("Applied Tool : " + tool.getName());

        }
    }

    public static ArrayList<ATRTool> getTools() {
        return tools;
    }
}
