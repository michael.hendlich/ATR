package lu.uni.lcsb.atr;

import ext.TextPrompt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class APIDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtFieldAddress;
    private JTextField txtFieldToken;
    private JCheckBox checkBoxSaveValues;
    private JTextField txtFieldUsername;
    private JPasswordField passFieldPassword;
    private JTextField txtFieldProjectID;

    public APIDialog(Window window) {
        super(window);
        setModalityType(ModalityType.APPLICATION_MODAL);
        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        if (DataProvider.redcapAddress != null && !DataProvider.redcapAddress.equals("")) {
            txtFieldAddress.setText(DataProvider.redcapAddress);
        } else {
            TextPrompt tp = new TextPrompt("ex : http://your_redcap_address/redcap/", txtFieldAddress);
            tp.changeAlpha(0.5f);
        }

        if (DataProvider.apiToken != null && !DataProvider.apiToken.equals("")) {
            txtFieldToken.setText(DataProvider.apiToken);
        }

        if (DataProvider.userName != null && !DataProvider.userName.equals("")) {
            txtFieldUsername.setText(DataProvider.userName);
        }

        if (DataProvider.redcapProjectID != null && !DataProvider.redcapProjectID.equals("")) {
            txtFieldProjectID.setText(DataProvider.redcapProjectID);
        }

        pack();
        setLocationRelativeTo(null);
    }

    private void onOK() {
        DataProvider.redcapAddress = txtFieldAddress.getText();
        DataProvider.apiToken = txtFieldToken.getText();
        DataProvider.redcapProjectID = txtFieldProjectID.getText();
        DataProvider.userName = txtFieldUsername.getText();
        DataProvider.password = passFieldPassword.getPassword();

        if (checkBoxSaveValues.isSelected()) {
            Settings.putRawValue("API_ADDRESS", DataProvider.redcapAddress);
            Settings.putRawValue("API_TOKEN", DataProvider.apiToken);
            Settings.putRawValue("PROJECT_ID", DataProvider.redcapProjectID);
            Settings.putRawValue("USER_NAME", DataProvider.userName);
            FileIO.saveSettings();
        }
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
