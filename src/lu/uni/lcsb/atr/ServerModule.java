package lu.uni.lcsb.atr;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sysadmin on 10.08.15.
 */
public class ServerModule {

    private HttpServer server;

    private String sessionToken = null;
    private boolean loggedIn = false;

    public ServerModule(String port) {

        int portNr = Integer.parseInt(port.substring(1));

        try {
            server = HttpServer.create(new InetSocketAddress(InetAddress.getLocalHost().getHostAddress(), portNr), 0);
            System.out.println("Started Server on " + server.getAddress());
        } catch (IOException e) {
            e.printStackTrace();
        }

        sessionToken = new SessionTokenGenerator().nextSessionToken();
        System.out.println("Session Token :");
        System.out.println(sessionToken);

        server.createContext("/atr_api", new APIHandler());
        server.setExecutor(null);
        server.start();
    }


    private class APIHandler implements HttpHandler {

        HashMap<String, String> request;

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            System.out.println("Incoming request");
            request = bodyToMap(httpExchange.getRequestBody());

            Response response = new Response("Invalid request", 400);

            if (get("token").equals(sessionToken)) {

                switch (get("action")) {
                    case ("connect") : {
                        response = connectAction();
                        break;
                    }
                    case ("create_project") : {
                        response = createProjectAction();
                        break;
                    }
                    case ("dl_dd") : {
                        response = downloadDD();
                        break;
                    }
                    case ("up_dd") : {
                        response = uploadDD();
                        break;
                    }
                    case ("apply_project") : {
                        response = applyProject();
                        break;
                    }
                    case ("apply_tool") : {
                        response = applyTool();
                        break;
                    }
                    case ("tool_info") : {
                        response = getToolInfo();
                        break;
                    }
                    case ("get_table") : {
                        response = getTable();
                        break;
                    }
                    case ("set_table") : {
                        response = setTable();
                        break;
                    }
                    case ("project_info") : {
                        response = getProjectInfo();
                        break;
                    }
                }

            } else {
                response = new Response("Invalid Token", 400);
            }

            httpExchange.sendResponseHeaders(response.getCode(), response.getBody().length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBody().getBytes());
            os.close();

        }

        private Response setTable() {
            String csvData = null;
            try {
                csvData = URLDecoder.decode(get("csv_data"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                return new Response("Invalid data format", 400);
            }
            if (csvData == null) {
                return new Response("Invalid request", 400);
            }

            int startLine;
            try {
                String sl = get("start_line");
                if (sl == null) {
                    return new Response("Invalid request", 400);
                }
                startLine = Integer.parseInt(sl);
            } catch (NumberFormatException ex) {
                return new Response("Invalid request", 400);
            }

            String projectName = get("project_name");
            if (projectName == null) {
                return new Response("Invalid request", 400);
            }

            boolean loadSuccessful = SharedFunctionality.loadProject(projectName);
            if (!loadSuccessful) {
                return new Response("Project file is invalid or does not exist", 400);
            }

            int lineCount;
            try {
                String lc = get("line_count");
                if (lc == null) {
                    return new Response("Invalid request", 400);
                }
                lineCount = Integer.parseInt(lc);
            } catch (NumberFormatException ex) {
                return new Response("Invalid request", 400);
            }

            String domain = get("domain");
            if (domain == null) {
                return new Response("Invalid request", 400);
            }
            domain = domain.toUpperCase();

            if (Utils.getNameSetByID(domain) == null) {
                return new Response("Domain not found", 400);
            }

            StringBuilder tableFileName = new StringBuilder();
            tableFileName.append("Projects/");
            tableFileName.append(projectName);
            tableFileName.append("/");
            tableFileName.append(domain);
            tableFileName.append("[");
            tableFileName.append(startLine);
            tableFileName.append("-");
            tableFileName.append(startLine + lineCount - 1);
            tableFileName.append("]");
            tableFileName.append(DataProvider.DEFAULT_CSV_FILE_EXTENSION);

            PrintWriter out = null;
            try {
                out = new PrintWriter(tableFileName.toString());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            out.println(csvData);
            out.close();


            List <String> metaLines;
            try {
                metaLines = Files.readAllLines(DataProvider.project.getSaveFile().toPath(), Charset.defaultCharset());
            } catch (IOException e) {
                return new Response("Project file is invalid or does not exist", 400);
            }

            String tnr = get("table_nr");
            if (tnr != null) {
                try {
                    int tableNr = Integer.parseInt(tnr);
                    if (tableNr > metaLines.size() - 1) {
                        return new Response("Invalid request", 400);
                    }

                    File f = new File(tableFileName.toString());
                    String name = f.getName();
                    metaLines.set(tableNr, name);

                } catch (NumberFormatException ex) {
                    return new Response("Invalid request", 400);
                }

            } else {

                File f = new File(tableFileName.toString());
                String name = f.getName();
                metaLines.add(name);

            }

            PrintWriter metaout = null;
            try {
                metaout = new PrintWriter(DataProvider.project.getSaveFile());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            for (String line : metaLines) {
                metaout.println(line);
            }
            metaout.close();

            SharedFunctionality.loadProject(projectName);
            SharedFunctionality.saveProject(projectName);

            return new Response("Successfully updated table", 200);


        }

        private Response getProjectInfo() {
            String projectName = get("project_name");
            if (projectName == null) {
                return new Response("Invalid request", 400);
            }
            boolean loadSuccessful = SharedFunctionality.loadProject(projectName);
            if (!loadSuccessful) {
                return new Response("Project file is invalid or does not exist", 400);
            }
            Project p = DataProvider.project;
            StringBuilder out = new StringBuilder();

            out.append("name=");
            out.append(p.getName());
            out.append("\n");

            out.append("tables=");
            out.append(p.getTables().size());
            out.append("\n");

            for (int i = 0; i < p.getTables().size(); i++) {
                SelectionTable t = p.getTables().get(i);

                out.append("table_nr=");
                out.append(i);
                out.append("\n");

                out.append("domain=");
                out.append(t.getAnnotationSet().getFieldNameSet().getID());
                out.append("\n");

                out.append("lines=");
                out.append(t.getAnnotationSet().getSize());
                out.append("\n");

                out.append("dd_start_line=");
                out.append(t.getSelection().getStartLine());
                out.append("\n");

                out.append("dd_end_line=");
                out.append(t.getSelection().getEndLine());
                out.append("\n");
            }

            return new Response(out.toString(), 200);
        }

        private Response getTable() {
            String projectName = get("project_name");
            if (projectName == null) {
                return new Response("Invalid request", 400);
            }
            int tableNr;
            try {
                tableNr = Integer.parseInt(get("table_nr"));
            } catch (NumberFormatException ex) {
                return new Response("Invalid request", 400);
            }

            boolean loadSuccessful = SharedFunctionality.loadProject(projectName);
            if (!loadSuccessful) {
                return new Response("Project file is invalid or does not exist", 400);
            }

            List<String> metaLines = null;
            try {
                metaLines = Files.readAllLines(DataProvider.project.getSaveFile().toPath(), Charset.defaultCharset());
            } catch (Exception e) {
                e.printStackTrace();
                return new Response("Project file is invalid", 400);
            }

            if (tableNr > metaLines.size() - 1) {
                return new Response("Invalid table number", 400);
            }

            String tableFileName = "Projects/" + projectName + "/" + metaLines.get(tableNr);
            File tableFile = new File(tableFileName);

            List<String> tableLines = null;
            try {
                tableLines = Files.readAllLines(tableFile.toPath(), Charset.defaultCharset());
            } catch (Exception e) {
                e.printStackTrace();
                return new Response("Project file is invalid", 400);
            }

            StringBuilder sb = new StringBuilder();
            for (String line : tableLines) {
                sb.append(line);
                sb.append("\n");
            }

            return new Response(sb.toString(), 200);
        }

        private Response getToolInfo() {
            String toolName = get("tool_name");
            if (toolName == null) {
                return new Response("Invalid request", 400);
            }
            ATRTool tool = Tools.findToolbyName(toolName);
            if (tool == null) {
                return new Response("Tool not found", 400);
            }
            String description = tool.getDescription();
            if (description == null || description.equals("")) {
                return new Response("Tool has no description", 400);
            }
            return new Response(description, 200);
        }

        private Response applyTool() {
            String toolName = get("tool_name");
            String projectName = get("project_name");
            String params = get("params");
            if (toolName == null || projectName == null) {
                return new Response("Invalid request", 400);
            }
            boolean loadSuccessful = SharedFunctionality.loadProject(projectName);
            if (!loadSuccessful) {
                return new Response("Project file is invalid or does not exist", 400);
            }
            SharedFunctionality.applyTool(toolName, ToolMode.valueOf(toolName), params);
            SharedFunctionality.saveProject(projectName);
            return new Response("Applied Tool", 200);
        }

        private Response uploadDD() {
            String pw = get("password");
            if (pw == null) {
                return new Response("Invalid request", 400);
            }
            DataProvider.password = pw.toCharArray();
            DataProvider.userName = get("username");
            if (DataProvider.userName == null) {
                return new Response("Invalid request", 400);
            }
            DataProvider.redcapProjectID = get("pid");
            if (DataProvider.redcapProjectID == null) {
                return new Response("Invalid request", 400);
            }
            try {
                DataProvider.redcapAddress = URLDecoder.decode(get("rc_address"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return new Response("Could not process REDCap URL", 400);
            }
            SharedFunctionality.uploadDD();
            return new Response("Uploaded DD", 200);
        }

        private Response applyProject() {
            String projectName = get("project_name");
            if (projectName == null) {
                return new Response("Invalid request", 400);
            }
            boolean loadSuccessful = SharedFunctionality.loadProject(projectName);
            if (!loadSuccessful) {
                return new Response("Project file is invalid or does not exist", 400);
            }
            if (DataProvider.dataDictionary == null || !DataProvider.dataDictionary.exists()) {
                return new Response("No valid Data Dictionary found", 400);
            }
            SharedFunctionality.createModifiedDD(DataProvider.project.getTables());
            return new Response("Output file modified", 200);
        }

        private Response downloadDD() {
            String redcapAddress = get("rc_address");
            if (redcapAddress == null) {
                return new Response("Invalid request", 400);
            }
            try {
                DataProvider.redcapAddress = URLDecoder.decode(redcapAddress, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return new Response("Could not process REDCap URL", 400);
            }
            DataProvider.apiToken = get("rc_token");
            if (DataProvider.apiToken == null) {
                return new Response("Invalid request", 400);
            }
            SharedFunctionality.downloadDD();
            String fileName = Utils.getNewestInFile().getName();
            return new Response("Downloaded : " + fileName, 200);
        }

        private Response createProjectAction() {
            String projectName = get("project_name");
            if (projectName == null) {
                return new Response("Invalid request", 400);
            }
            File file = new File("Projects/" + projectName + "/" + projectName + DataProvider.DEFAULT_METADATA_FILE_EXTENSION);
            if (file.exists()) {
                return new Response("A project with this name already exists", 400);
            }
            SharedFunctionality.createProject(projectName, file);
            return new Response("Project created", 200);
        }

        private Response connectAction() {
            if (get("token").equals(sessionToken)) {
                loggedIn = true;
                return new Response("Successfully connected", 200);
            } else {
                return new Response("error", 400);
            }
        }

        private String get(String key) {
            return request.get(key);
        }


        private HashMap<String, String> bodyToMap(InputStream requestBody){
            HashMap<String, String> result = new HashMap<>();

            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(requestBody));
                String line;
                while((line = in.readLine()) != null) {
                    for (String param : line.split("&")) {
                        String pair[] = param.split("=");
                        if (pair.length>1) {
                            result.put(pair[0], pair[1]);
                        }else{
                            result.put(pair[0], "");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            return result;
        }

    }

    private class Response {

        private String body;
        private int code;

        public Response(String body, int code) {
            this.body = body;
            this.code = code;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }


    public final class SessionTokenGenerator {
        private SecureRandom random = new SecureRandom();

        public String nextSessionToken() {
            return new BigInteger(130, random).toString(32);
        }
    }

}
