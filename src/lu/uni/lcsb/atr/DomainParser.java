package lu.uni.lcsb.atr;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;

/**
 * Created by sysadmin on 25.08.15.
 */
public class DomainParser {

    static ArrayList<String> requirementTokens = new ArrayList<String>() {{
        add("Req");
        add("Perm");
        add("Exp");
    }};
    static String core = "Core";


    public static boolean parseDomains() {
        File folder = new File("domains_to_import");
        boolean converted = false;
        for (final File file : folder.listFiles()) {
            if (file.isFile()) {
                if (parse(file)) {
                    converted = true;
                    System.out.println("Converted " + file.getName());
                }
            }
        }
        return converted;
    }

    private static boolean parse(File file) {
        String domainName = Utils.stripFileExtension(file.getName()).toUpperCase();

        if (!Utils.domainAlreadyLoaded(domainName)) {
            ArrayList<String> lines = null;
            try {
                lines = (ArrayList<String>) Files.readAllLines(file.toPath(), Charset.defaultCharset());
            } catch (IOException e) {
                e.printStackTrace();
            }

            FieldNameSet set = createSet(lines, domainName);
            FileIO.saveDomain(set);
            return true;
        }
        return false;
    }

    public static FieldNameSet createSet(ArrayList<String> lines, String name) {
        ArrayList<FieldName> entries = new ArrayList<>();

        ArrayList<String> block = new ArrayList<>();
        boolean inBlock = true;
        for (String line : lines) {
            if (inBlock) {
                block.add(line);
                if (requirementTokens.contains(line)) {
                    entries.add(createEntry(block));
                    block = new ArrayList<>();
                } else if (line.equals(core)) {
                    block = new ArrayList<>();
                }
            }
        }
        return new FieldNameSet(entries, name);
    }

    private static FieldName createEntry(ArrayList<String> block) {
        String name = block.get(0);
        String description = block.get(block.size() - 2);
        boolean required = false;
        if (block.get(block.size() - 1).equals("Req")) {
            required = true;
        }
        return new FieldName(name, required, description);
    }


}
