package lu.uni.lcsb.atr;

import com.google.common.util.concurrent.SimpleTimeLimiter;
import com.google.common.util.concurrent.TimeLimiter;
import com.google.common.util.concurrent.UncheckedTimeoutException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by sysadmin on 05.08.15.
 */
public class APIConnector {

    public static void downloadDD() {
        HttpClient httpclient = new DefaultHttpClient();
        //HttpPost httppost = new HttpPost("http://212.69.168.211/~hacettpe/redcap/api/");
        HttpPost httppost = new HttpPost(DataProvider.redcapAddress + "api/");

        try {

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("action", "export"));
            nameValuePairs.add(new BasicNameValuePair("token", DataProvider.apiToken));
            nameValuePairs.add(new BasicNameValuePair("content", "metadata"));
            nameValuePairs.add(new BasicNameValuePair("format", "csv"));
            //nameValuePairs.add(new BasicNameValuePair("type", "flat"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpclient.execute(httppost);
            BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line;

            ArrayList<String> lines = new ArrayList<>();
            while((line = in.readLine()) != null) {
                lines.add(line);
            }

            if (response.getStatusLine().getStatusCode() == 200) {
                PrintWriter pw = null;
                String fileName = "backups/in/" + Utils.getTimestamp() + ".csv";
                try {
                    pw = new PrintWriter(new FileWriter(fileName));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                for (String s : lines) {
                    pw.println(s);
                }
                pw.close();
                System.out.println("Downloaded : " + fileName);

            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("Download failed : ");
                sb.append("\n");
                for (String s : lines) {
                    System.out.println(s);
                    sb.append(s);
                    sb.append("\n");
                }
                JOptionPane.showMessageDialog(null, sb.toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static boolean uploadDDSafe() {
        TimeLimiter limiter = new SimpleTimeLimiter();
        Uploader up = new Uploader();
        UploaderInterface proxy = limiter.newProxy(
                up, UploaderInterface.class, 15, TimeUnit.SECONDS);
        try {
            return proxy.uploadDD();
        } catch (UncheckedTimeoutException e) {
            System.err.println("Upload timeout");
            return false;
        }
    }

    public interface UploaderInterface{
        boolean uploadDD();
    }

    static class Uploader implements UploaderInterface {
        public boolean uploadDD() {
            WebDriver driver = null;
            try {
                driver = new FirefoxDriver();

                driver.get(DataProvider.redcapAddress + "index.php");

                WebElement elementUsername = driver.findElement(By.id("username"));
                elementUsername.sendKeys(DataProvider.userName);

                WebElement elementPassword = driver.findElement(By.id("password"));
                elementPassword.sendKeys(new String(DataProvider.password));
                elementPassword.submit();

                driver.get(DataProvider.redcapAddress + "redcap_v6.8.2/Design/data_dictionary_upload.php?pid=" + DataProvider.redcapProjectID);

                List<WebElement> inputs = driver.findElements(By.xpath("//input"));
                WebElement fileInput = inputs.get(0);
                fileInput.sendKeys(new File("backups/out/out.csv").getAbsolutePath());

                driver.findElement(By.id("submit")).click();

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    //e.printStackTrace();
                }

                driver.findElement(By.name("commit")).click();

                driver.close();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            } finally {
                if (driver != null) {
                    driver.close();
                }
            }
            return true;

        }
    }




}
