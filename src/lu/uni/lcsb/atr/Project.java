package lu.uni.lcsb.atr;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by sysadmin on 31.07.15.
 */
public class Project {

    private ArrayList<SelectionTable> tables;
    private String name;
    private File saveFile;

    public Project(ArrayList<SelectionTable> tables, String name, File saveFile) {
        if (tables == null) {
            this.tables = new ArrayList<>();
        } else {
            this.tables = tables;
        }
        this.name = name;
        this.saveFile = saveFile;
    }

    public ArrayList<SelectionTable> getTables() {
        return tables;
    }

    public void setTables(ArrayList<SelectionTable> tables) {
        this.tables = tables;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getSaveFile() {
        return saveFile;
    }

    public void setSaveFile(File saveFile) {
        this.saveFile = saveFile;
    }
}
