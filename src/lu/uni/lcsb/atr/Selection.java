package lu.uni.lcsb.atr;

/**
 * Created by sysadmin on 30.07.15.
 */
public class Selection {

    private int startLine;
    private int endLine;

    private FieldNameSet nameSet;

    public Selection(int startLine, int endLine, FieldNameSet nameSet) {
        this.startLine = startLine;
        this.endLine = endLine;
        this.nameSet = nameSet;
    }

    public int getStartLine() {
        return startLine;
    }

    public int getEndLine() {
        return endLine;
    }

    FieldNameSet getNameSet() {
        return nameSet;
    }

    public int getSize() {
        return (endLine - startLine) + 1;
    }

    public void setNameSet(FieldNameSet nameSet) {
        this.nameSet = nameSet;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[ ");
        sb.append(startLine);
        sb.append(" - ");
        sb.append(endLine);
        sb.append(" ]  ");
        sb.append(nameSet.getID());
        return sb.toString();
    }
}
