package lu.uni.lcsb.atr;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sysadmin on 03.08.15.
 */
public class Settings {

    /*
    Settings keys:

    PREV_PROJECT (abs path)
    DD_LOCATION
    API_ADDRESS
    API_TOKEN
    PROJECT_ID
    USER_NAME

    */

    private static HashMap<String, String> vars = new HashMap<>();
    private static ArrayList<String> previousProjects = new ArrayList<>();

    public static HashMap<String, String> getRawVars() {
        return vars;
    }

    public static void putRawValue(String name, String value) {
        if (name.equals("PREV_PROJECT")) {
            previousProjects.add(value);
        } else {
            vars.put(name, value);
        }
    }

    public static String getRawValue(String name) {
        return vars.get(name);
    }

    public static void apply() {
        if (vars.get("DD_LOCATION") != null) {
            File f = new File(vars.get("DD_LOCATION"));
            if (f.exists()) {
                DataProvider.dataDictionary = f;
            } else {
                vars.remove("DD_LOCATION");
            }
        }
        if (vars.get("API_ADDRESS") != null) {
            DataProvider.redcapAddress = vars.get("API_ADDRESS");
        }
        if (vars.get("API_TOKEN") != null) {
            DataProvider.apiToken = vars.get("API_TOKEN");
        }
        if (vars.get("PROJECT_ID") != null) {
            DataProvider.redcapProjectID = vars.get("PROJECT_ID");
        }
        if (vars.get("USER_NAME") != null) {
            DataProvider.userName = vars.get("USER_NAME");
        }
        ArrayList<String> toDelete = new ArrayList<>();
        for (String s : previousProjects) {
            File file = new File(s);
            if (file.exists()) {
                DataProvider.previousProjects.add(file);
            } else {
                toDelete.add(s);
            }
        }
        for (String s : toDelete) {
            previousProjects.remove(s);
        }
        FileIO.saveSettings();

    }

    public static ArrayList<String> getPreviousProjects() {
        return previousProjects;
    }

    public static boolean alreadyContainsPreviousProject(String abspath) {
        for (String s : previousProjects) {
            if (s.equals(abspath)) {
                return true;
            }
        }
        return false;
    }
}
