package lu.uni.lcsb.atr;

import javax.swing.*;

class UIFlow {

    public static void main(String[] args) {

        FileIO.loadSettings();
        Settings.apply();
        DataProvider.loadedNameSets = FileIO.loadAllDomains();
        boolean newDomainsConverted = DomainParser.parseDomains();
        if (newDomainsConverted) {
            DataProvider.loadedNameSets = FileIO.loadAllDomains();
        }
        Tools.registerTools();

        if (args.length == 0 || args[0].equals("-gui")) {
            init();
            uiLoop();
        } else if (args[0].equals("-server")) {
            new ServerModule(args[1]);
        }

    }

    private static void uiLoop() {
        boolean running = true;
        FlowCode flowCode =  FlowCode.welcomeScreen;
        Frame frame = null;
        while (running) {
            switch (flowCode) {
                case loadProject: {
                    LoadProjectDialog dialog = new LoadProjectDialog(null);
                    dialog.setVisible(true);
                    flowCode = dialog.getFlowcode();
                    break;
                }
                case openAnnotationWindow:
                    if (frame != null) {
                        frame.dispose();
                    }
                    frame = new Frame();
                    frame.setVisible(true);
                    flowCode = FlowCode.nothing;
                    break;
                case exit:
                    running = false;
                    break;
                case newProject: {
                    NewProjectDialog dialog = new NewProjectDialog(null);
                    dialog.setVisible(true);
                    flowCode = dialog.getFlowCode();
                    if (flowCode ==  FlowCode.exit && frame != null) {
                        flowCode = FlowCode.nothing;
                    }
                    break;
                }
                case loadDD:
                    DataDictionaryDialog ddDialog = new DataDictionaryDialog(null);
                    ddDialog.setVisible(true);
                    flowCode = ddDialog.getFlowCode();
                    break;
                case welcomeScreen: {
                    WelcomeDialog dialog = new WelcomeDialog(null);
                    dialog.setVisible(true);
                    flowCode = dialog.getFlowCode();
                    break;
                }
                default:
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (frame != null) {
                        flowCode = frame.getFlowCode();
                        frame.setFlowCode(FlowCode.nothing);
                    }
                    break;
            }
        }
    }

    private static void init() {
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        }
        catch (Exception e) {
            System.out.println("???");
        }
    }
}
