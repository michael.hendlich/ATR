package lu.uni.lcsb.atr;

/**
 * Created by sysadmin on 24.07.15.
 */

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import javax.swing.*;


public class FileIO {

    public static void loadSettings() {
        File file = new File(DataProvider.DEFAULT_SETTINGS_DIRECTORY_NAME + File.separator + DataProvider.DEFAULT_SETTINGS_FILE_NAME + DataProvider.DEFAULT_SETTINGS_FILE_EXTENSION);

        if (file.exists()) {

            List<String> lines = null;
            try {
                lines = Files.readAllLines(file.toPath(), Charset.defaultCharset());
            } catch (IOException e) {
                e.printStackTrace();
            }

            for (String line : lines) {
                String[] splitted = line.split("=", -1);
                Settings.putRawValue(splitted[0], splitted[1]);
            }

            System.out.println("Loaded Settings");

        }
    }

    public static void saveSettings() {
        File dir = new File(DataProvider.DEFAULT_SETTINGS_DIRECTORY_NAME);
        if (!dir.exists()) {
            dir.mkdir();
        }

        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new FileWriter(DataProvider.DEFAULT_SETTINGS_DIRECTORY_NAME + File.separator + DataProvider.DEFAULT_SETTINGS_FILE_NAME + DataProvider.DEFAULT_SETTINGS_FILE_EXTENSION));
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Map.Entry<String, String> entry : Settings.getRawVars().entrySet()) {
            pw.print(entry.getKey());
            pw.print('=');
            pw.print(entry.getValue());
            pw.println();
        }

        for (String s : Settings.getPreviousProjects()) {
            pw.print("PREV_PROJECT");
            pw.print('=');
            pw.print(s);
            pw.println();
        }

        pw.close();
        System.out.println("Saved Settings");
    }

    public static void saveProject(File metaDataFile) {

        if (DataProvider.project.getSaveFile() != null) {
            deleteProjectFiles();
        }

        DataProvider.project.setSaveFile(metaDataFile);

        File dir = metaDataFile.getParentFile();
        if (!dir.exists()) {
            dir.mkdir();
        }
        ArrayList<File> annotationFiles = new ArrayList<>();

        if (DataProvider.project.getTables() != null) {
            for (SelectionTable table : DataProvider.project.getTables()) {

                StringBuilder sb = new StringBuilder();
                sb.append(table.getAnnotationSet().getFieldNameSet().getID());
                sb.append("[");
                sb.append(table.getSelection().getStartLine());
                sb.append("-");
                sb.append(table.getSelection().getEndLine());
                sb.append("]");
                sb.append(DataProvider.DEFAULT_CSV_FILE_EXTENSION);

                String fileName = sb.toString();

                File file = new File(dir, fileName);
                annotationFiles.add(file);
                writeAnnotationSet(table.getAnnotationSet(), file);
            }
        }

        writeMetadata(annotationFiles, metaDataFile);

        if (!Settings.alreadyContainsPreviousProject(metaDataFile.getAbsolutePath())) {
            Settings.putRawValue("PREV_PROJECT", metaDataFile.getAbsolutePath());
            saveSettings();
        }

        System.out.println("Saved Project : " + metaDataFile.getAbsolutePath());
    }

    private static void deleteProjectFiles() {
        File oldMetaFile = DataProvider.project.getSaveFile();

        List<String> metaFileLines = null;
        try {
            metaFileLines = Files.readAllLines(oldMetaFile.toPath(), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String line : metaFileLines) {
            new File(line).delete();
        }

        oldMetaFile.delete();
    }

    public static Project loadProject(File metaFile) {

        ArrayList<SelectionTable> result = new ArrayList<>();

        String projectName = Utils.stripFileExtension(metaFile.getName());

        for (File file : loadAllAssociatedFiles(metaFile)) {

            AnnotationSet annotationSet = loadAnnotationSet(file);
            FieldNameSet nameSet = annotationSet.getFieldNameSet();

            String fileName = Utils.stripFileExtension(file.getName());
            char[] chars = fileName.toCharArray();

            int pointer = 0;

            while (chars[pointer] != '[') {
                pointer++;
            }

            pointer++;

            StringBuilder sbStart = new StringBuilder();
            while (chars[pointer] != '-') {
                sbStart.append(chars[pointer]);
                pointer++;
            }
            int startLine = Integer.parseInt(sbStart.toString());

            pointer++;

            StringBuilder sbEnd = new StringBuilder();
            while (chars[pointer] != ']') {
                sbEnd.append(chars[pointer]);
                pointer++;
            }
            int endLine = Integer.parseInt(sbEnd.toString());

            Selection selection = new Selection(startLine, endLine, nameSet);
            SelectionTable table = new SelectionTable(selection, annotationSet);
            result.add(table);

        }

        if (!Settings.alreadyContainsPreviousProject(metaFile.getAbsolutePath())) {
            Settings.putRawValue("PREV_PROJECT", metaFile.getAbsolutePath());
            saveSettings();
        }

        System.out.println("Loaded Project : " + metaFile.getAbsolutePath());
        return new Project(result, projectName, metaFile);
    }

    private static ArrayList<File> loadAllAssociatedFiles(File metaFile) {

        ArrayList<File> result = new ArrayList<>();

        List<String> metaFileLines = null;
        try {
            metaFileLines = Files.readAllLines(metaFile.toPath(), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String line : metaFileLines) {
            result.add(new File(metaFile.getParentFile().getAbsolutePath() + File.separator + line));
        }

        return result;
    }

    private static void writeMetadata(ArrayList<File> annotationFiles, File file) {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new FileWriter(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (File f : annotationFiles) {
            pw.println(f.getName());
        }

        pw.close();
    }

    public static void writeAnnotationSet(AnnotationSet set, File file) {

        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(DataProvider.NEW_LINE_SEPARATOR);

        try {

            fileWriter = new FileWriter(file);
            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

            String[] fileHeader = new String[set.getFieldNameSet().getSize()];
            for (int i = 0; i < fileHeader.length; i++) {
                fileHeader[i] = set.getFieldNameSet().getFieldNames().get(i).getName();
            }
            csvFilePrinter.printRecord(fileHeader);

            for (Annotation a : set.getList()) {
                List record = new ArrayList();
                for (Tuple t : a.getTuples()) {
                    record.add(t.getRight());
                }
                csvFilePrinter.printRecord(record);
            }

            System.out.println("CSV file was created successfully");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
                csvFilePrinter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter/csvPrinter");
                e.printStackTrace();
            }
        }

    }

    public static AnnotationSet loadAnnotationSet(File file) {

        FileReader fileReader = null;
        CSVParser csvFileParser = null;

        AnnotationSet result = null;

        try {

            fileReader = new FileReader(file);

            List<String> lines = null;
            try {
                lines = Files.readAllLines(file.toPath(), Charset.defaultCharset());
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "ERROR : Couldn't read file.");
                e.printStackTrace();
                return null;
            }

            String header = lines.get(0);
            String[] varNamesArray = header.split(",");
            List<String> varNamesList = Arrays.asList(varNamesArray);

            FieldNameSet usedNameSet = null;
            for (FieldNameSet set : DataProvider.loadedNameSets) {
                if (set.hasEqualNames(varNamesList)) {
                    usedNameSet = set;
                    break;
                }
            }

            CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(varNamesArray);
            csvFileParser = new CSVParser(fileReader, csvFileFormat);

            List csvRecords = csvFileParser.getRecords();

            if (usedNameSet == null) {
                System.out.println("No fitting domain found");
                return null;
            }

            result = new AnnotationSet(usedNameSet);

            for (int i = 1; i < csvRecords.size(); i++) {
                CSVRecord record = (CSVRecord) csvRecords.get(i);
                Annotation a = new Annotation(usedNameSet);

                for (String left : varNamesArray) {
                    String right = record.get(left);
                    Tuple t = new Tuple(left, right, usedNameSet);
                    a.insertTuple(t);
                }

                result.addAnnotation(a);

            }

        } catch (Exception e) {
            System.err.println("Error in FileIO");
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
                csvFileParser.close();
                return result;
            } catch (IOException e) {
                System.err.println("Error while closing fileReader/csvFileParser");
                e.printStackTrace();
            }
        }


        return null;
    }

    public static ArrayList<ArrayList<String>> readCSVFile(File file) {

        String extension = "";

        // wtf
        /*
        int in = file.getName().lastIndexOf('.');
        if (in > 0) {
            extension = file.getName().substring(in+1);
        }

        if (!extension.equals(".csv")) {
            JOptionPane.showMessageDialog(null, "ERROR : Only accepts CSV files");
            return null;
        }


        */
        FileReader fileReader = null;

        CSVParser csvFileParser = null;

        //Create the CSVFormat object with the header mapping
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader();

        ArrayList<ArrayList<String>> lines = null;
        try {

            //Create a new list of student to be filled by CSV file data
            lines = new ArrayList<>();

            //initialize FileReader object
            fileReader = new FileReader(file);

            //initialize CSVParser object
            csvFileParser = new CSVParser(fileReader, csvFileFormat);

            //Get a list of CSV file records
            List csvRecords = csvFileParser.getRecords();


            final Map<String, Integer> headerMap = csvFileParser.getHeaderMap();
            ArrayList<String> headerList = new ArrayList<>();
            for (String s : headerMap.keySet()) {
                headerList.add(s);
            }
            DataProvider.dataDictionaryHeaderMapping = new String[headerList.size()];
            for (int i = 0; i < headerList.size(); i++) {
                DataProvider.dataDictionaryHeaderMapping[i] = headerList.get(i);
            }


            //Read the CSV file records starting from the second record to skip the header
            for (int i = 0; i < csvRecords.size(); i++) {
                CSVRecord record = (CSVRecord) csvRecords.get(i);
                ArrayList<String> line = new ArrayList<>();
                for (int k = 0; k < DataProvider.dataDictionaryHeaderMapping.length; k++) {
                    line.add(record.get(DataProvider.dataDictionaryHeaderMapping[k]));
                }
                lines.add(line);
            }

        } catch (Exception e) {
            System.err.println("Error in FileIO");
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
                csvFileParser.close();
            } catch (IOException e) {
                System.err.println("Error while closing fileReader/csvFileParser");
                e.printStackTrace();
            }
        }
        return lines;

    }

    public static boolean modifyLoadedCSVFile(ArrayList<ArrayList<String>> lines, ArrayList<String> stringsToInsert, int startLine) {
        //return true if overwritten
        boolean result = false;
        for (int i = 0; i < stringsToInsert.size(); i++) {
            String entry = lines.get(i + startLine).get(DataProvider.columnToInsert);
            if (!entry.equals("") && entry != null) {
                result = true;
            }
            lines.get(i + startLine).set(DataProvider.columnToInsert, stringsToInsert.get(i));
        }
        return result;
    }

    public static void writeCSVFile(ArrayList<ArrayList<String>> lines, File file) {

        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;

        //Create the CSVFormat object with "\n" as a record delimiter
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(DataProvider.NEW_LINE_SEPARATOR);

        try {

            //initialize FileWriter object
            fileWriter = new FileWriter(file);

            //initialize CSVPrinter object
            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

            //Create CSV file header
            csvFilePrinter.printRecord(DataProvider.dataDictionaryHeaderMapping);

            for (ArrayList<String> line : lines) {
                csvFilePrinter.printRecord(line);
            }

            System.out.println("CSV file was created successfully");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
                csvFilePrinter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter/csvPrinter");
                e.printStackTrace();
            }
        }
    }

    public static void saveDomain(FieldNameSet set) {
        File dir = new File(DataProvider.DEFAULT_NAMESET_DIRECTORY_NAME);
        if (!dir.exists()) {
            dir.mkdir();
        }

        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;

        String fileName = DataProvider.DEFAULT_NAMESET_DIRECTORY_NAME + File.separator + set.getID() + DataProvider.DEFAULT_NAMESET_FILE_EXTENSION;
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(DataProvider.NEW_LINE_SEPARATOR);

        try {

            fileWriter = new FileWriter(fileName);
            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
            csvFilePrinter.printRecord(DataProvider.domainHeaderMapping);

            //Write a new student object list to the CSV file
            for (FieldName fn : set.getFieldNames()) {
                ArrayList<String> record = new ArrayList<>();
                record.add(fn.getName());
                if (fn.isRequired()) {
                    record.add("y");
                } else {
                    record.add("n");
                }
                record.add(fn.getDescription());
                csvFilePrinter.printRecord(record);
            }

            System.out.println("CSV file was created successfully");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
                csvFilePrinter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter/csvPrinter");
                e.printStackTrace();
            }
        }

    }

    public static ArrayList<FieldNameSet> loadAllDomains() {
        ArrayList<FieldNameSet> result = new ArrayList<>();

        File dir = new File(DataProvider.DEFAULT_NAMESET_DIRECTORY_NAME + File.separator);
        File[] directoryListing = dir.listFiles();
        Arrays.sort(directoryListing);
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (child.isFile()) {
                    System.out.println("Loaded Domain : " + child);

                    FileReader fileReader = null;

                    CSVParser csvFileParser = null;

                    //Create the CSVFormat object with the header mapping
                    CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(DataProvider.domainHeaderMapping);

                    try {

                        ArrayList<FieldName> fieldNames = new ArrayList<>();

                        fileReader = new FileReader(child);
                        csvFileParser = new CSVParser(fileReader, csvFileFormat);
                        List csvRecords = csvFileParser.getRecords();

                        for (int i = 1; i < csvRecords.size(); i++) {
                            CSVRecord record = (CSVRecord) csvRecords.get(i);
                            String name = record.get("Name");
                            boolean required = record.get("Required").equals("y");
                            String description = record.get("Description");
                            FieldName fn = new FieldName(name, required, description);
                            fieldNames.add(fn);
                        }

                        result.add(new FieldNameSet(fieldNames, Utils.stripFileExtension(child.getName()).toUpperCase()));
                    } catch (Exception e) {
                        System.out.println("Error in CsvFileReader !!!");
                        e.printStackTrace();
                    } finally {
                        try {
                            fileReader.close();
                            csvFileParser.close();
                        } catch (IOException e) {
                            System.out.println("Error while closing fileReader/csvFileParser !!!");
                            e.printStackTrace();
                        }
                    }


                }
            }

        } else {
            System.err.println("No domains to import");
        }
        return result;
    }

    public static void importNewNameSetFile() {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (file.getAbsolutePath().endsWith(DataProvider.DEFAULT_NAMESET_FILE_EXTENSION)) {

                File target = new File(DataProvider.DEFAULT_NAMESET_DIRECTORY_NAME + File.separator + file.getName());

                try {
                    Files.copy(file.toPath(), target.toPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                DataProvider.loadedNameSets = loadAllDomains();
                JOptionPane.showMessageDialog(null, "Permanently imported domain file. You can now delete the source file.");

            } else {
                JOptionPane.showMessageDialog(null, "Wrong file type");
            }

        }

    }
}

