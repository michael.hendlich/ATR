package lu.uni.lcsb.atr;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class LoadProjectDialog extends JDialog {
    private JPanel contentPane;
    private JButton btnLoadPrevious;
    private JButton btnLoadExplicit;
    private JTextField txtFieldFilePath;
    private JButton btnSearchFile;
    private JComboBox comboBoxPrevProjects;
    private JButton buttonOK;
    private JButton buttonCancel;

    public FlowCode flowcode;

    File toLoad;

    public LoadProjectDialog(Window window) {

        super(window);
        setModalityType(ModalityType.APPLICATION_MODAL);

        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        initBtnSearchFile();
        initBtnLoadExplicit();
        initBtnLoadPrevious();
        initComboBox();
        pack();
        setLocationRelativeTo(null);
    }

    private void initComboBox() {
        for (File file : DataProvider.previousProjects) {
            comboBoxPrevProjects.addItem(Utils.stripFileExtension(file.getName()));
        }
    }

    private void initBtnLoadPrevious() {
        btnLoadPrevious.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                File file = DataProvider.previousProjects.get(comboBoxPrevProjects.getSelectedIndex());
                SharedFunctionality.loadProject(file);
                flowcode = FlowCode.openAnnotationWindow;
                dispose();
            }
        });
    }

    private void initBtnLoadExplicit() {
        btnLoadExplicit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                String ddText = txtFieldFilePath.getText();
                if (ddText != null) {
                    toLoad = new File(ddText);
                    if (!toLoad.exists()) {
                        JOptionPane.showMessageDialog(LoadProjectDialog.this, "Invalid file path");
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(LoadProjectDialog.this, "Invalid file path");
                    return;
                }


                SharedFunctionality.loadProject(toLoad);
                flowcode = FlowCode.openAnnotationWindow;
                dispose();
            }
        });
    }

    private void initBtnSearchFile() {
        btnSearchFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File("Projects"));
                fileChooser.addChoosableFileFilter(new ATRFileFilter());
                fileChooser.setAcceptAllFileFilterUsed(true);


                if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    toLoad = fileChooser.getSelectedFile();
                    txtFieldFilePath.setText(toLoad.getAbsolutePath());
                }

            }
        });
    }

    public FlowCode getFlowcode() {
        return flowcode;
    }

    private void onCancel() {
        flowcode = FlowCode.exit;
        dispose();
    }
}
