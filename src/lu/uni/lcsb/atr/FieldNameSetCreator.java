package lu.uni.lcsb.atr;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Scanner;

public class FieldNameSetCreator extends JDialog {
    private JPanel contentPane;
    private JButton btnOK;
    private JButton btnCancel;
    private JTextField txtFieldName;
    private JTextField txtFieldColumns;
    private JLabel lblInsertNames;
    private JLabel lblInsertName;
    private JTextArea txtArea;
    private JButton btnTest;
    private JLabel lbl;

    public FieldNameSetCreator(Window window) {

        super(window);
        setModalityType(ModalityType.APPLICATION_MODAL);
        setAlwaysOnTop(true);

        setContentPane(contentPane);
        setTitle("Create Domain");
        getRootPane().setDefaultButton(btnOK);

        btnOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        pack();
        setLocationRelativeTo(null);
        initBtnTest();
    }

    private void initBtnTest() {
        btnTest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String name = txtFieldName.getText();
                if (name == null || name.equals("")) {
                    JOptionPane.showMessageDialog(FieldNameSetCreator.this, "No name given");
                    return;
                }
                name = name.toUpperCase();
                String in = txtArea.getText();
                ArrayList<String> lines = new ArrayList<>();
                Scanner scanner = new Scanner(in);
                while (scanner.hasNextLine()) {
                    lines.add(scanner.nextLine());
                }
                scanner.close();
                FieldNameSet set = DomainParser.createSet(lines, name);
                DataProvider.loadedNameSets.add(set);
                FileIO.saveDomain(set);
                dispose();
            }
        });
    }


    private void onOK() {
        if (!txtFieldName.getText().equals("") && !txtFieldColumns.getText().equals("")) {
            String[] tokens = txtFieldColumns.getText().split(",");
            ArrayList<FieldName> names = new ArrayList<>();
            for (String s : tokens) {
                names.add(new FieldName(s, false, null));
            }
            FieldNameSet set = new FieldNameSet(names, txtFieldName.getText());
            DataProvider.loadedNameSets.add(set);
            FileIO.saveDomain(set);
            dispose();
        }
    }

    private void onCancel() {
        dispose();
    }

}
