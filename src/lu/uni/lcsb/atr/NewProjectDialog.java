package lu.uni.lcsb.atr;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class NewProjectDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JButton btnSearchDD;
    private JTextField txtFieldDDPath;
    private JTextField txtFieldName;
    private JButton btnSearchProject;
    private JTextField txtFieldProjectPath;
    private JButton btnSetupAPI;
    private JRadioButton radioBtnLocal;
    private JRadioButton radioBtnAPI;
    private JTextField txtFieldAPIAddress;

    private FlowCode flowCode;

    private File ddFile;
    private File projectFolder;

    public NewProjectDialog(Window window) {

        super(window);
        setModalityType(ModalityType.APPLICATION_MODAL);

        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        initTxtFieldDDPath();
        initTxtFieldName();
        initBtnSearchDD();
        initBtnSearchProject();
        initRadioButtons();
        initBtnSetupAPI();

        if (DataProvider.redcapAddress != null && !DataProvider.redcapAddress.equals("")) {
            txtFieldAPIAddress.setText(DataProvider.redcapAddress);
        }

        pack();
        setLocationRelativeTo(null);
    }

    private void initTxtFieldName() {
        txtFieldName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                update();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                update();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                update();
            }

            private void update() {
                String text = txtFieldName.getText();
                projectFolder = new File("Projects/" + text + "/");
                txtFieldProjectPath.setText(projectFolder.getAbsolutePath());
            }
        });
    }

    private void initRadioButtons() {

        //default
        radioBtnLocal.setSelected(true);
        txtFieldAPIAddress.setEnabled(false);
        btnSetupAPI.setEnabled(false);
        txtFieldDDPath.setEnabled(true);
        btnSearchDD.setEnabled(true);


        radioBtnLocal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                txtFieldAPIAddress.setEnabled(false);
                btnSetupAPI.setEnabled(false);
                txtFieldDDPath.setEnabled(true);
                btnSearchDD.setEnabled(true);
            }
        });

        radioBtnAPI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                txtFieldAPIAddress.setEnabled(true);
                btnSetupAPI.setEnabled(true);
                txtFieldDDPath.setEnabled(false);
                btnSearchDD.setEnabled(false);
            }
        });
    }

    private void initBtnSetupAPI() {
        btnSetupAPI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                APIDialog dialog = new APIDialog(null);
                dialog.setVisible(true);
                if (DataProvider.redcapAddress != null) {
                    txtFieldAPIAddress.setText(DataProvider.redcapAddress);
                }
            }
        });
    }

    private void initTxtFieldDDPath() {
        String ddLocation = Settings.getRawValue("DD_LOCATION");
        txtFieldDDPath.setText(ddLocation);
        if (ddLocation != null && !ddLocation.equals("")) {
            ddFile = new File(Settings.getRawValue("DD_LOCATION"));
        }
        txtFieldDDPath.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                update();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                update();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                update();
            }

            private void update() {
                String text = txtFieldName.getText();
                projectFolder = new File("Projects/" + text + "/");
                txtFieldProjectPath.setText(projectFolder.getAbsolutePath());
            }
        });

    }

    private void initBtnSearchProject() {
        btnSearchProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setAcceptAllFileFilterUsed(false);
                if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    projectFolder = fileChooser.getSelectedFile();
                    txtFieldProjectPath.setText(projectFolder.getAbsolutePath());
                }
            }
        });
    }

    private void initBtnSearchDD() {
        btnSearchDD.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.addChoosableFileFilter(new CSVFileFilter());
                fileChooser.setAcceptAllFileFilterUsed(true);
                if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    ddFile = fileChooser.getSelectedFile();
                    txtFieldDDPath.setText(ddFile.getAbsolutePath());
                }
            }
        });
    }

    private void onOK() {


        if (txtFieldName.getText() == null || txtFieldName.getText().equals("") || txtFieldProjectPath == null || txtFieldProjectPath.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Please specify a project name and save location");
        }else{

            flowCode = FlowCode.loadDD;

            String projectText = txtFieldProjectPath.getText();
            if (projectText != null) {
                projectFolder = new File(projectText);
                projectFolder.mkdir();
                if (!projectFolder.isDirectory()) {
                    JOptionPane.showMessageDialog(this, "Project location has to be a folder");
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(this, "Invalid project location");
                return;
            }


            if (!ddFile.getName().endsWith(".csv")) {
                JOptionPane.showMessageDialog(this, "Invalid data dictionary");
                return;
            }

            if (radioBtnLocal.isSelected()) {

                String ddText = txtFieldDDPath.getText();
                if (ddText != null) {
                    ddFile = new File(ddText);
                    if (!ddFile.exists()) {
                        JOptionPane.showMessageDialog(this, "Invalid data dictionary");
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Invalid data dictionary");
                    return;
                }

                Settings.putRawValue("DD_LOCATION", ddFile.getAbsolutePath());
                FileIO.saveSettings();

            } else if (radioBtnAPI.isSelected()) {
                SharedFunctionality.downloadDD();
                ddFile = Utils.getNewestInFile();
            }

            DataProvider.dataDictionary = ddFile;
            String projectName = txtFieldName.getText();
            File projectFile = new File(projectFolder.getAbsolutePath() + File.separator + projectName + DataProvider.DEFAULT_METADATA_FILE_EXTENSION);

            if (projectFile.exists()) {
                int res = JOptionPane.showConfirmDialog(this, "File already exists. Overwrite?", "Warning", JOptionPane.YES_NO_OPTION);
                if (res == JOptionPane.NO_OPTION) {
                    return;
                }
            }

            SharedFunctionality.createProject(projectName, projectFile);
            boolean validDD = SharedFunctionality.loadDD();

            if (validDD) {
                dispose();
            } else {
                JOptionPane.showMessageDialog(this, "Invalid Data Dictionary");
            }
        }


    }

    private void onCancel() {
        flowCode = FlowCode.exit;
        dispose();
    }

    public FlowCode getFlowCode() {
        return flowCode;
    }

}
