package lu.uni.lcsb.atr;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sysadmin on 29.07.15.
 */
public class DataProvider {

    public static final int DEFAULT_COLUMN_TO_INSERT = 17;
    public static int columnToInsert = DEFAULT_COLUMN_TO_INSERT;
    public static boolean alwaysOverwrite = false;
    public static final String NEW_LINE_SEPARATOR = "\n";

    public static String apiToken = "";
    public static String redcapAddress = "";
    public static String redcapProjectID = "";
    public static String userName = "";
    public static char[] password;

    public static File dataDictionary;
    public static ArrayList<ArrayList<String>> dataDictionaryEntries = null;

    public static Project project;

    public static ArrayList<File> previousProjects = new ArrayList<>();

    public static String[] dataDictionaryHeaderMapping = {"Variable / Field Name","Form Name","Section Header","Field Type","Field Label","Choices, Calculations, OR Slider Labels","Field Note","Text Validation Type OR Show Slider Number","Text Validation Min","Text Validation Max","Identifier?","Branching Logic (Show field only if...)","Required Field?","Custom Alignment","Question Number (surveys only)","Matrix Group Name","Matrix Ranking?","Field Annotation"};
    public static String[] domainHeaderMapping = {"Name", "Required", "Description"};

    public static ArrayList<FieldNameSet> loadedNameSets = new ArrayList<>();

    public static HashMap<FieldNameSet, Color> colorAssignments = new HashMap<>();

    //public static final Color[] COLOR_PALETTE = {new Color(123,255,109), new Color(232,230,12), new Color(255,138,0), new Color(232,12,82), new Color(47,0,255)};

    public static final Color[] COLOR_PALETTE = {Color.decode("#1be7ff"), Color.decode("#6eeb83"), Color.decode("#e4ff1a"), Color.decode("#e8aa14"), Color.decode("#ff5714")};

    public static final String DEFAULT_NAMESET_FILE_EXTENSION = ".csv";
    public static final String DEFAULT_METADATA_FILE_EXTENSION = ".atr";
    public static final String DEFAULT_CSV_FILE_EXTENSION = ".csv";
    public static final String DEFAULT_SETTINGS_FILE_EXTENSION = ".cfg";

    public static final String DEFAULT_SETTINGS_FILE_NAME = "piv";

    public static final String DEFAULT_NAMESET_DIRECTORY_NAME = "domains";
    public static final String DEFAULT_SETTINGS_DIRECTORY_NAME = "settings";
}
