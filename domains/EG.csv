Name,Required,Description
STUDYID,y,Unique identifier for a study.
DOMAIN,y,Two-character abbreviation for the domain.
USUBJID,y,Identifier used to uniquely identify a subject across all studies for all applications or submissions involving the product.
EGSEQ,y,Sequence Number given to ensure uniqueness of subject records within a domain. May be any valid number.
EGGRPID,n,Used to tie together a block of related records in a single domain for a subject.
EGREFID,n,Internal or external ECG identifier. Example: UUID.
EGSPID,n,Sponsor-defined reference number. Perhaps pre-printed on the CRF as an explicit line identifier or defined in the sponsor's operational database. Example: Line number from the ECG page.
EGTESTCD,y,"Short name of the measurement, test, or examination described in EGTEST. It can be used as a column name when converting a dataset from a vertical to a horizontal format. The value in EGTESTCD cannot be longer than 8 characters, nor can it start with a number (e.g., “1TEST”). EGTESTCD cannot contain characters other than letters, numbers, or underscores. Examples :PRMEAN, QTMEAN"
EGTEST,y,"Verbatim name of the test or examination used to obtain the measurement or finding. The value in EGTEST cannot be longer than 40 characters. Examples: Summary (Mean) PR Duration, Summary (Mean) QT Duration"
EGCAT,n,"Used to categorize ECG observations across subjects. Examples: MEASUREMENT, FINDING, INTERVAL"
EGSCAT,n,A further categorization of the ECG.
EGPOS,n,"Position of the subject during a measurement or examination. Examples: SUPINE, STANDING, SITTING."
EGORRES,n,"Result of the ECG measurement or finding as originally received or collected. Examples of expected values are 62 or 0.151 when the result is an interval or measurement, or “ATRIAL FIBRILLATION” or “QT PROLONGATION” when the result is a finding."
EGORRESU,n,Original units in which the data were collected. The unit for EGORRES. Examples: sec or msec.
EGSTRESC,n,"Contains the result value for all findings, copied or derived from EGORRES in a standard format or standard units. EGSTRESC should store all results or findings in character format; if results are numeric, they should also be stored in numeric format in EGSTRESN. For example, if a test has results of “NONE”, “NEG”, and “NEGATIVE” in EGORRES and these results effectively have the same meaning, they could be represented in standard format in EGSTRESC as “NEGATIVE”. For other examples, see general assumptions. Additional examples of result data: SINUS BRADYCARDIA, ATRIAL FLUTTER, ATRIAL FIBRILLATION."
EGSTRESN,n,Used for continuous or numeric results or findings in standard format; copied in numeric format from EGSTRESC. EGSTRESN should store all numeric test results or findings.
EGSTRESU,n,Standardized unit used for EGSTRESC or EGSTRESN.
EGSTAT,n,"Used to indicate an ECG was not done, or an ECG measurement was not taken. Should be null if a result exists in EGORRES."
EGREASND,n,Describes why a measurement or test was not performed. Examples: BROKEN EQUIPMENT or SUBJECT REFUSED. Used in conjunction with EGSTAT when value is NOT DONE.
EGFXN,n,File name and path for the external ECG Waveform file.
EGNAM,n,Name or identifier of the laboratory or vendor who provided the test results.
EGLEAD,n,"The lead used for the measurement, examples, V1, V6, aVR, I, II, III."
EGMETHOD,n,Method of the ECG test. Examples: 12 LEAD STANDARD.
EGBLFL,n,Indicator used to identify a baseline value. The value should be “Y” or null.
EGDRVFL,n,"Used to indicate a derived record. The value should be Y or null. Records which represent the average of other records, or that do not come from the CRF, or are not as originally collected or received are examples of records that would be derived for the submission datasets. If EGDRVFL=Y, then EGORRES could be null, with EGSTRESC, and (if numeric) EGSTRESN having the derived value."
EGEVAL,n,"Role of the person who provided the evaluation. Used only for results that are subjective (e.g., assigned by a person or a group). Should be null for records that contain collected or derived data. Examples: INVESTIGATOR, ADJUDICATION COMMITTEE, VENDOR."
VISITNUM,n,"1. Clinical encounter number. 2. Numeric version of VISIT, used for sorting."
VISIT,n,1. Protocol-defined description of clinical encounter. 2. May be used in addition to VISITNUM and/or VISITDY.
VISITDY,n,Planned study day of the visit based upon RFSTDTC in Demographics.
EGDTC,n,Date of ECG.
EGDY,n,"1. Study day of the ECG, measured as integer days. 2. Algorithm for calculations must be relative to the sponsor-defined RFSTDTC variable in Demographics."
EGTPT,n,"1. Text Description of time when measurement should be taken. 2. This may be represented as an elapsed time relative to a fixed reference point, such as time of last dose. See EGTPTNUM and EGTPTREF. Examples: Start, 5 min post."
EGTPTNUM,n,Numerical version of EGTPT to aid in sorting.
EGELTM,n,"Planned elapsed time (in ISO 8601) relative to a fixed time point reference (EGTPTREF). Not a clock time or a date time variable. Represented as an ISO 8601 duration. Examples: “-PT15M” to represent the period of 15 minutes prior to the reference point indicated by EGTPTREF, or “PT8H” to represent the period of 8 hours after the reference point indicated by EGTPTREF."
EGTPTREF,n,"Name of the fixed reference point referred to by EGELTM, EGTPTNUM, and EGTPT. Examples: PREVIOUS DOSE, PREVIOUS MEAL."
EGRFTDTC,n,"Date/time of the reference time point, EGTPTREF."
