Name,Required,Description
STUDYID,y,Unique identifier for a study.
DOMAIN,y,Two-character abbreviation for the domain.
USUBJID,y,Identifier used to uniquely identify a subject across all studies for all applications or submissions involving the product.
ECSEQ,y,Sequence Number given to ensure uniqueness of subject records within a domain. May be any valid number.
ECGRPID,n,Used to tie together a block of related records in a single domain for a subject.
ECREFID,n,"Internal or external identifier (e.g., kit number, bottle label, vial identifier)."
ECSPID,n,Sponsor-defined reference number. Perhaps pre-printed on the CRF as an explicit line identifier or defined in the sponsor’s operational database. Example: Line number on a CRF Page.
ECLNKID,n,Identifier used to link related records across domains.
ECLNKGRP,n,"Identifier used to link related, grouped records across domains."
ECTRT,y,Name of the intervention treatment known to the subject and/or administrator.
ECMOOD,n,"Mode or condition of the record specifying whether the intervention (activity) is intended to happen or has happened. Values align with BRIDG pillars (e.g., scheduled context, performed context) and HL7 activity moods (e.g., intent, event). Examples: SCHEDULED, PERFORMED. CDISC Controlled Terminology request for MOOD codelist with values of SCHEDULED and PERFORMED has been submitted."
ECCAT,n,Used to define a category of related ECTRT values.
ECSCAT,n,A further categorization of ECCAT values.
ECPRESP,n,Used when a specific intervention is pre-specified. Values should be “Y” or null.
ECOCCUR,n,"Used to indicate whether a treatment occurred when information about the occurrence is solicited. ECOCCUR = ‘N’ when a treatment was not taken, not given, or missed."
ECDOSE,n,Amount of ECTRT when numeric. Not populated when ECDOSTXT is populated.
ECDOSTXT,n,Amount of ECTRT when non-numeric. Dosing amounts or a range of dosing information collected in text form. Example: 200-400. Not populated when ECDOSE is populated.
ECDOSU,n,"Units for ECDOSE, ECDOSTOT, or ECDOSTXT."
ECDOSFRM,n,"Dose form for ECTRT. Examples: TABLET, LOTION."
ECDOSFRQ,n,"Usually expressed as the number of repeated administrations of ECDOSE within a specific time period. Examples: Q2H, QD, BID."
ECDOSTOT,n,Total daily dose of ECTRT using the units in ECDOSU. Used when dosing is collected as Total Daily Dose.
ECDOSRGM,n,"Text description of the intended schedule or regimen for the Intervention. Example: TWO WEEKS ON, TWO WEEKS OFF."
ECROUTE,n,"Route of administration for the intervention. Examples: ORAL, INTRAVENOUS."
ECLOT,n,Lot Number of the ECTRT product.
ECLOC,n,"Specifies location of administration. Example: ARM, LIP."
ECLAT,n,"Qualifier for anatomical location further detailing laterality of the intervention administration. Examples: LEFT, RIGHT."
ECDIR,n,"Qualifier for anatomical location further detailing directionality. Examples: ANTERIOR, LOWER, PROXIMAL, UPPER."
ECPORTOT,n,"Qualifier for anatomical location further detailing distribution, which means arrangement of, apportioning of. Examples: ENTIRE, SINGLE, SEGMENT."
ECFAST,n,"Indicator used to identify fasting status. Examples: Y, N."
ECPSTRG,n,"Amount of an active ingredient expressed quantitatively per dosage unit, per unit of volume, or per unit of weight, according to the pharmaceutical dose form."
ECPSTRGU,n,"Unit for ECPSTRG. Examples: mg/TABLET, mg/mL."
ECADJ,n,Describes reason or explanation of why a dose is adjusted.
EPOCH,n,"Trial Epoch of the exposure as collected record. Examples: RUN-IN, TREATMENT."
ECSTDTC,n,The date/time when administration of the treatment indicated by ECTRT and ECDOSE began.
ECENDTC,n,"The date/time when administration of the treatment indicated by ECTRT and ECDOSE ended. For administrations considered given at a point in time (e.g., oral tablet, pre-filled syringe injection), where only an administration date/time is collected, ECSTDTC should be copied to ECENDTC as the standard representation."
ECSTDY,n,Study day of ECSTDTC relative to the sponsor-defined DM.RFSTDTC.
ECENDY,n,Study day of ECENDTC relative to the sponsor-defined DM.RFSTDTC.
ECDUR,n,Collected duration of administration represented in ISO 8601 duration format. Used only if collected on the CRF and not derived from start and end date/times.
ECTPT,n,"This may be represented as an elapsed time relative to a fixed reference point, such as time of last dose. See ECTPTNUM and ECTPTREF."
ECTPTNUM,n,Numerical version of ECTPT to aid in sorting.
ECELTM,n,Planned elapsed time (in ISO 8601 format) relative to the planned fixed reference (ECTPTREF). This variable is useful where there are repetitive measures. Not a clock time. Represented in ISO 8601 duration format.
ECTPTREF,n,"Name of the fixed reference point referred to by ECELTM, ECTPTNUM, and ECTPT. Examples: PREVIOUS DOSE, PREVIOUS MEAL."
ECRFTDTC,n,Date/time for a fixed reference time point defined by ECTPTREF in ISO 8601 character format.
