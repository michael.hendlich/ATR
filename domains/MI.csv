Name,Required,Description
STUDYID,y,Unique identifier for a study.
DOMAIN,y,Two-character abbreviation for the domain.
USUBJID,y,Identifier used to uniquely identify a subject across all studies for all applications or submissions involving the product.
MISEQ,y,Sequence number given to ensure uniqueness of subject records within a domain. May be any valid number.
MIGRPID,n,Used to tie together a block of related records in a single domain for a subject. This is not the treatment group number.
MIREFID,n,Internal or external specimen identifier. Example: Specimen barcode number.
MISPID,n,Sponsor-defined reference number. Perhaps pre-printed on the CRF as an explicit line identifier or defined in the sponsor's operational database. Example: Line number from the MI Findings page.
MITESTCD,y,"Short name of the measurement, test, or examination described in MITEST. It can be used as a column name when converting a dataset from a vertical to a horizontal format. The value in MITESTCD cannot be longer than 8 characters, nor can it start with a number (e.g., “1TEST” is not valid). MITESTCD cannot contain characters other than letters, numbers, or underscores. Examples: HER2, BRCA1, TTF1."
MITEST,y,"Verbatim name of the test or examination used to obtain the measurement or finding. The value in MITEST cannot be longer than 40 characters. Examples: Human Epidermal Growth Factor Receptor 2, Breast Cancer Susceptibility Gene 1, Thyroid Transcription Factor 1."
MITSTDTL,n,"Further description of the test performed in producing the MI result. This would be used to represent specific attributes, such as intensity score or percentage of cells displaying presence of the biomarker or compound"
MICAT,n,Used to define a category of related records.
MISCAT,n,Used to define a further categorization of MICAT.
MIORRES,n,Result of the histopathology measurement or finding as originally received or collected.
MIORRESU,n,Original unit for MIORRES.
MISTRESC,n,"Contains the result value for all findings, copied or derived from MIORRES in a standard format or standard units. MISTRESC should store all results or findings in character format; if results are numeric, they should also be stored in numeric format in MISTRESN."
MISTRESN,n,Used for continuous or numeric results or findings in standard format; copied in numeric format from MISTRESC. MISTRESN should store all numeric test results or findings.
MISTRESU,n,Standardized unit used for MISTRESC and MISTRESN.
MIRESCAT,n,Examples: MALIGNANT or BENIGN for tumor findings.
MISTAT,n,Used to indicate examination not done or result is missing. Should be null if a result exists in MIORRES or have a value of NOT DONE when MIORRES=NULL.
MIREASND,n,"Reason not done. Used in conjunction with MISTAT when value is NOT DONE. Examples: SAMPLE AUTOLYZED, SPECIMEN LOST."
MINAM,n,"Name or identifier of the vendor (e.g., laboratory) that provided the test results."
MISPEC,y,"Subject of the observation. Defines the type of specimen used for a measurement. Examples: TISSUE, BLOOD, BONE MARROW."
MISPCCND,n,Free or standardized text describing the condition of the specimen. Example: AUTOLYZED.
MILOC,n,"Location relevant to the collection of the specimen. Examples: LUNG, KNEE JOINT, ARM, THIGH."
MILAT,n,"Qualifier for laterality of the location of the specimen in MILOC. Examples: LEFT, RIGHT, BILATERAL."
MIDIR,n,"Qualifier for directionality of the location of the specimen in MILOC. Examples: DORSAL, PROXIMAL."
MIMETHOD,n,"Method of the test or examination. This could include the technique or type of staining used for the slides. Examples: IHC, Crystal Violet, Safranin, Trypan Blue, or Propidium Iodide."
MIBLFL,n,Indicator used to identify a baseline value. The value should be “Y” or null.
MIEVAL,n,"Role of the person who provided the evaluation. Example: PATHOLOGIST, PEER REVIEW, SPONSOR PATHOLOGIST."
VISITNUM,n,"1. Clinical encounter number. 2. Numeric version of VISIT, used for sorting."
VISIT,n,1. Protocol-defined description of clinical encounter. 2. May be used in addition to VISITNUM and/or VISITDY.
VISITDY,n,Planned study day of the visit based upon RFSTDTC in Demographics.
MIDTC,n,"Date/time of specimen collection, in ISO 8601 format."
MIDY,n,"Study day of specimen collection, in integer days. The algorithm for calculations must be relative to the sponsor-defined RFSTDTC variable in the Demographics (DM) domain."
